<?php

Auth::routes();


Route::get('/', 'HomeController@commande')->name('dashboard');

Route::get('/dashboard', 'HomeController@commande')->name('dashboard.display');


Route::resource('users', 'UserController');

Route::match(['put', 'patch'],'users/changePass/{user}', 'UserController@updatePass')->name('users.updatePass');
Route::match(['put', 'patch'],'users/changeLogo/{user}', 'UserController@updateLogo')->name('users.updateLogo');


Route::resource('roles', 'RoleController');

Route::resource('permissions', 'PermissionController');

Route::resource('posts', 'PostController');

Route::resource('articles', 'ArticleController');


Route::get('/commandes/{nombre_semaine?}/{nombre_anne?}', 'CommandesController@semaine')->name('commandes');
Route::get('/commandes-historique/{nombre_semaine?}', 'CommandesController@historiqueSemaine')->name('historique.commandes');
Route::post('/commande/create', 'CommandesController@store')->name('commande.create');
Route::post('/commande/create2', 'CommandesController@store2')->name('commande.create2');

Route::get('/previsions/{anne?}/{nombre_mois?}', 'PrevisionsController@mois')->name('previsions');
Route::post('/previsions/create', 'PrevisionsController@store')->name('previsions.create');

Route::get('/previsions_hebdo/{anne?}/{nombre_mois?}', 'PrevisionsController@mois2')->name('previsions_hebdo');
Route::post('/previsions/create2', 'PrevisionsController@store2')->name('previsions.create2');

Route::post('/previsions/save_repartition', 'PrevisionsController@saveRepartition')->name('repartition.save');

/*** For ERP **/
Route::get('/getcommandes/{last_update}', 'ErpController@getCommandes')->name('erp.commandes');
Route::get('/getprevisions/{last_update}', 'ErpController@getPrevisions')->name('erp.previsions');

Route::get('/historique-log/{id_user?}', 'LogsController@getHistorque')->name('file.log');


