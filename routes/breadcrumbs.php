<?php

// Home
Breadcrumbs::register('dashboard', function($breadcrumbs)
{
    $breadcrumbs->push('Tableaux de bord', route('dashboard'));
});

Breadcrumbs::register('dashboard.display', function($breadcrumbs)
{
    $breadcrumbs->push('Tableaux de bord', route('dashboard.display'));
});


// Users
Breadcrumbs::register('users.index', function($breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Utilisateurs', route('users.index'));
});

// Users > Add User
Breadcrumbs::register('users.create', function($breadcrumbs)
{
    $breadcrumbs->parent('users.index');
    $breadcrumbs->push('Ajouter utilisateur', route('users.create'));
});

// Users > Edit User
Breadcrumbs::register('users.edit', function($breadcrumbs, $user)
{
    $breadcrumbs->parent('users.index');
    $breadcrumbs->push('Modifier utilisateur ' . $user->name, route('users.edit', $user->id));
});


// Roles
Breadcrumbs::register('roles.index', function($breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Roles', route('roles.index'));
});

// Roles > Add Role
Breadcrumbs::register('roles.create', function($breadcrumbs)
{
    $breadcrumbs->parent('roles.index');
    $breadcrumbs->push('Ajouter Role', route('roles.create'));
});

// Roles > Edit Role
Breadcrumbs::register('roles.edit', function($breadcrumbs, $role)
{
    $breadcrumbs->parent('roles.index');
    $breadcrumbs->push('Modifier Role ' . $role->name, route('roles.edit', $role->id));
});


// Roles
Breadcrumbs::register('permissions.index', function($breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Autorisations disponibles', route('permissions.index'));
});

// Roles > Add Role
Breadcrumbs::register('permissions.create', function($breadcrumbs)
{
    $breadcrumbs->parent('permissions.index');
    $breadcrumbs->push(' Ajouter autorisation', route('permissions.create'));
});

// Roles > Edit Role
Breadcrumbs::register('permissions.edit', function($breadcrumbs, $permission)
{
    $breadcrumbs->parent('permissions.index');
    $breadcrumbs->push('Modifier autorisation ' . $permission->name, route('permissions.edit', $permission->id));
});



// Articles
Breadcrumbs::register('articles.index', function($breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Tous les articles', route('articles.index'));
});

// Users > Add User
Breadcrumbs::register('articles.create', function($breadcrumbs)
{
    $breadcrumbs->parent('articles.index');
    $breadcrumbs->push("Ajouter un article", route('articles.create'));
});

// Users > Edit User
Breadcrumbs::register('articles.edit', function($breadcrumbs, $article)
{
    $breadcrumbs->parent('articles.index');
    $breadcrumbs->push("Modifier l'article " . $article->reference, route('articles.edit', $article->id));
});


Breadcrumbs::register('commandes', function($breadcrumbs, $nombre_semaine=null)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push("Commandes semaine " . $nombre_semaine, route('commandes', $nombre_semaine));

});

Breadcrumbs::register('historique.commandes', function($breadcrumbs, $nombre_semaine=null)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push("Historique Commandes semaine " . $nombre_semaine, route('historique.commandes', $nombre_semaine));

});


Breadcrumbs::register('previsions', function($breadcrumbs, $nombre_mois=null)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push("Planning Besoin", route('previsions', ['nombre_mois' => $nombre_mois]));

});

Breadcrumbs::register('previsions_hebdo', function($breadcrumbs, $nombre_mois=null)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push("Répartition hebdo ", route('previsions_hebdo', ['nombre_mois' => $nombre_mois] ));

});


// Historique
Breadcrumbs::register('file.log', function($breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Historique', route('file.log'));
});
