<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArticlesActuels extends Model
{
    protected $table = 'articles_actuels';

    protected $fillable = [
        'code_client', 'article_id', 'conditionnement', 'stock_actuel_boraplast'
    ];


    public function article()
    {
        return $this->belongsTo('\App\Article');
    }
}
