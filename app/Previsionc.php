<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Previsionc extends Model
{
    protected $table = 'previsionsc';

    protected $fillable = [
        'code_client', 'article_id', 'nombre_palettes', 'date_commande', 'confirmation', 'conditionnement', 'stock_actuel_boraplast'
    ];


    public function article()
    {
        return $this->belongsTo('\App\Article');
    }
}
