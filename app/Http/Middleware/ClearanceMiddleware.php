<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class ClearanceMiddleware {
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        if (Auth::user()->hasPermissionTo('Administer roles & permissions')) //If user has this //permission
        {
            return $next($request);
        }

        if ($request->is('posts/create'))//If user is creating a post
        {
            if (!Auth::user()->hasPermissionTo('Create Post'))
            {
                abort('401');
            }
            else {
                return $next($request);
            }
        }

        if ($request->is('posts/*/edit')) //If user is editing a post
        {
            if (!Auth::user()->hasPermissionTo('Edit Post')) {
                abort('401');
            } else {
                return $next($request);
            }
        }

        if ( $request->is('posts/*') && $request->isMethod('Delete')) //If user is deleting a post
        {
            if (!Auth::user()->hasPermissionTo('Delete Post')) {
                abort('401');
            }
            else
            {
                return $next($request);
            }
        }


        /*********/

        if ($request->is('articles'))//If user is creating a post
        {
            if (!Auth::user()->hasPermissionTo('Articles'))
            {
                abort('401');
            }
            else {
                return $next($request);
            }
        }

        if ($request->is('articles/create'))//If user is creating a post
        {
            if (!Auth::user()->hasPermissionTo('Create Article'))
            {
                abort('401');
            }
            else {
                return $next($request);
            }
        }

        if ($request->is('articles/*/edit')) //If user is editing a post
        {
            if (!Auth::user()->hasPermissionTo('Edit Article')) {
                abort('401');
            } else {
                return $next($request);
            }
        }

        if ($request->is('articles/*') && $request->isMethod('Delete')) //If user is deleting a post
        {
            if (!Auth::user()->hasPermissionTo('Delete Article')) {
                abort('401');
            }
            else
            {
                return $next($request);
            }
        }


        return $next($request);
    }
}