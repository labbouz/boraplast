<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Session;

use App\User;
use App\ArticlesActuels;
use App\Commande;
use App\Prevision;

use Date;

use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }


    public function commande()
    {
        Date::setLocale('fr');

        if( auth()->check() && auth()->user()->hasRole('Admin') ) {
            /*****************************/

            $users = User::where('code_client', '<>', null)->get();

            $articles_actuels =  ArticlesActuels::all();

            $week = (int) date('W');
            $date = new Date(strtotime(date('Y').'W'.sprintf("%02d", $week )));
            $date_debut_semaine = $date->format('Y-m-d');
            $date_fin_semaine = $date->addDay(6)->format('Y-m-d');

            $mois = (int) date('n');
            $date_month = Carbon::create(null, $mois, 1, 0, 0, 0);

            $date_first_day_of_month = $date_month->format('Y-m-d');
            $date_last_day_of_month = $date_month->endOfMonth()->format('Y-m-d');

            $mois = $date_month->format('m/Y');


            $commandes_semaine = Commande::where('confirmation', '=', 1)
                ->where('date_commande', '>=', $date_debut_semaine)
                ->where('date_commande', '<=', $date_fin_semaine)
                ->get();

            $commandes_mois = Commande::where('confirmation', '=', 1)
                ->where('date_commande', '>=', $date_first_day_of_month)
                ->where('date_commande', '<=', $date_last_day_of_month)
                ->get();

            foreach ($users as $user) {
                $user->articles =  ArticlesActuels::where('code_client', '=', $user->code_client)->get();
                $user->commandes = Commande::where('code_client', '=', $user->code_client)
                                ->where('date_commande', '>=', $date_debut_semaine)
                                ->where('date_commande', '>=', $date_debut_semaine)
                                ->where('confirmation', '=', 1)
                                ->get();



                $user->previsions = Prevision::where('code_client', '=', $user->code_client)
                    ->where('date_semaine', '=', $week)
                    ->where('date_year', '=', date('Y'))
                    ->get();

            }


            /*****************************/

            return view('dashboard', compact('users', 'articles_actuels', 'commandes_semaine', 'commandes_mois', 'week', 'mois'));
        } else {
            if( auth()->check() && auth()->user()->hasRole('Client 2') ) {
                return redirect()->route('previsions', ['anne' => date('Y'), 'nombre_mois' => date('n')] );
            } else {
                return redirect()->route('commandes');
            }
        }




    }
}
