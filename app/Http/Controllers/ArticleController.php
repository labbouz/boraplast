<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Article;
use Auth;
use Session;

class ArticleController extends Controller
{
    public function __construct() {
        $this->middleware(['auth', 'clearance']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = Article::orderby('reference', 'asc')->paginate(100); //show only 5 items at a time in descending order

        return view('articles.index', compact('articles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('articles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validating title and body field
        $this->validate($request, [
            'reference'=>'required|max:100',
            'designation' =>'required',
            'unite' =>'required',
        ]);


        $article = Article::create($request->only('reference', 'designation', 'unite'));

        //Display a successful message upon save
        return redirect()->route('articles.index')
            ->with('flash_message', 'Article,
             '. $article->reference.' bien ajouté');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $article = Article::findOrFail($id);

        return view('articles.edit', compact('article'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'reference'=>'required|max:100',
            'designation' =>'required',
            'unite' =>'required',
        ]);

        $article = Article::findOrFail($id);
        $article->reference = $request->input('reference');
        $article->designation = $request->input('designation');
        $article->unite = $request->input('unite');
        $article->save();

        return redirect()->route('articles.index')->with('flash_message',
            'Article, '. $article->reference.' bien modifié');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $article = Article::findOrFail($id);
        $article->delete();

        return redirect()->route('articles.index')
            ->with('flash_message',
                'Article bien supprimé');
    }
}
