<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Session;

use App\Article;
use App\ArticlesActuels;
use App\Commande;
use App\Prevision;
use App\Previsionc;

use Date;

use Carbon\Carbon;

use App\User;
use App\Trace;

class LogsController extends Controller
{


    public function getHistorque($id_user=null)
    {

        $articles = Article::all();

        $jour=array();
        $jour[1] = 'LUN';
        $jour[2] = 'MAR';
        $jour[3] = 'MER';
        $jour[4] = 'JEU';
        $jour[5] = 'VEN';
        $jour[6] = 'SAM';
        $jour[7] = 'DIM';


        if($id_user==null) {
            $id_user = Auth::id();
        }


        $user = User::find($id_user);


        $traces = Trace::where('user_id', $id_user)
            ->orderBy('created_at', 'desc')->get();

        return view('logs', compact('traces', 'articles', 'jour', 'user'));

    }

}
