<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Session;

use App\ArticlesActuels;
use App\Prevision;
use App\Previsionc;
use App\Commande;

use Mail;

use Date;

use Carbon\Carbon;

use App\User;
use App\Trace;


class PrevisionsController extends Controller
{
    public function __construct() {
        $this->middleware(['auth', 'clearance']);
    }

    public function mois($anne=null, $nombre_mois=null) {

        $mois = (int) date('n');

        $mois_controle = $mois + 1 ;

        if(!$anne || !$nombre_mois || $nombre_mois > $mois_controle ) {
            return redirect()->route('previsions', ['anne' => date('Y'),  'nombre_mois' => $mois]);
        }

        $activation_update = 1;
        if( $anne == date('Y') &&  $nombre_mois < $mois  ) {
            $activation_update = 0;
        }


        Date::setLocale('fr');

        $articles_actuels = ArticlesActuels::where('code_client', Auth::user()->code_client)->get();

        $previsions = array();
        $data_previsions_total = array();
        $data_confirmation = array();


        $date_first_day_of_month = Carbon::create($anne, $nombre_mois, 1, 0, 0, 0);

        $date = $this->getWeeks( $date_first_day_of_month->toDateString(), $nombre_mois, 1 );


        $date_month = new Date($date_first_day_of_month);

        if(count($articles_actuels)>0) {
            foreach ($articles_actuels as $article_actuel) {
                $article_actuel->reference=$article_actuel->article->reference;
                $article_actuel->designation=$article_actuel->article->designation;
                $article_actuel->unite=$article_actuel->article->unite;

            }
            $articles_actuels->sortBy("reference");

            //$date_month = Carbon::create(null, $nombre_mois, 1, 0, 0, 0);



            $first_article = $articles_actuels->first();
            $date_last_update = Carbon::createFromFormat('Y-m-d H:i:s',$first_article->updated_at);


            foreach ($date['schedule'] as $date_semaine) {

                $z = $date_semaine['semaine'];

                $previsions_semaine_all = Prevision::where('code_client', '=', Auth::user()->code_client)
                    ->where('date_semaine', '=', $date_semaine['semaine'] )
                    ->where('date_year', '=', $date_semaine['anne'] )
                    ->get();

                $previsions_semaine = $previsions_semaine_all->unique('article_id');

                if( count($previsions_semaine) > 0 ) {
                    $data_confirmation[$z] = 1;
                } else {
                    $data_confirmation[$z] = 0;
                }

                foreach ($previsions_semaine as $prevision) {
                    $previsions[ $prevision->article_id ][ $date_semaine['semaine'] ] = $prevision->nombre_palettes;

                    if( !array_key_exists($z, $data_previsions_total) ) {

                        $data_previsions_total[$z] = array(
                            'nombre_palettes' => $prevision->nombre_palettes,
                            'quantittes' => $prevision->nombre_palettes * $prevision->conditionnement
                        );

                    } else {
                        $total_nombre_palettes = $data_previsions_total[$z]['nombre_palettes'] + $prevision->nombre_palettes ;
                        $total_quantittes = $data_previsions_total[$z]['quantittes'] + ( $prevision->nombre_palettes * $prevision->conditionnement );

                        $data_previsions_total[$z] = array(
                            'nombre_palettes' => $total_nombre_palettes,
                            'quantittes' => $total_quantittes
                        );


                    }
                }

            }
        } else {

            $date_last_update = Carbon::create();
        }



        return view('previsions', compact('articles_actuels', 'date_month', 'date_last_update','date', 'nombre_mois', 'previsions', 'data_previsions_total', 'activation_update', 'anne') );



    }

    public function mois2($anne=null, $nombre_mois=null) {

        $mois = (int) date('n');

        $mois_controle = $mois + 1 ;

        if(!$anne || !$nombre_mois || $nombre_mois > $mois_controle ) {
            return redirect()->route('previsions', ['anne' => date('Y'),  'nombre_mois' => $mois]);
        }

        $activation_update = 1;

        $activation_update = 1;
        if( $anne == date('Y') &&  $nombre_mois < $mois  ) {
            $activation_update = 0;
        }


        Date::setLocale('fr');

        $articles_actuels = ArticlesActuels::where('code_client', Auth::user()->code_client)->get();


        if(count($articles_actuels)==0) {
          echo "Il adapter le compte selon le code navision";
          exit();
        }


        foreach ($articles_actuels as $article_actuel) {
            $article_actuel->reference=$article_actuel->article->reference;
            $article_actuel->designation=$article_actuel->article->designation;
            $article_actuel->unite=$article_actuel->article->unite;

        }
        $articles_actuels->sortBy("reference");

        //$date_month = Carbon::create(null, $nombre_mois, 1, 0, 0, 0);

        $date_first_day_of_month = Carbon::create($anne, $nombre_mois, 1, 0, 0, 0);

        $date = $this->getWeeks( $date_first_day_of_month->toDateString(), $nombre_mois, 1 );


        $date_month = new Date($date_first_day_of_month);

        $first_article = $articles_actuels->first();
        $date_last_update = Carbon::createFromFormat('Y-m-d H:i:s',$first_article->updated_at);

        $previsions = array();
        $data_previsions_total = array();
        $data_confirmation = array();

        $jours_semaine = array();

        $data_journee_by_semaine = array();
        $data_journee_commande_prevision = array();
        $input_journee_commande_activier = array();

        /*
         * controle les jours
         */
        $days_commande = Auth::user()->days_commande;
        $days_semaines = array('lundi','mardi','mercredi','jeudi','vendredi','samedi','dimanche');

        foreach ($date['schedule'] as $date_semaine) {
            $z = $date_semaine['semaine'];

            $detail_dates = array();
            $jours_disponible = explode('#', $days_commande);

            if(strlen($days_commande)>0) {


                $date_current = Date::now();

                foreach ($jours_disponible as $jour_disponible) {
                    $key_data_semaine = array_search($jour_disponible,$days_semaines);

                    $date_temporaire = new Date(strtotime($date_semaine['anne'].'W'.sprintf("%02d", $date_semaine['semaine'])));

                    $cible_date = $date_temporaire->addDay($key_data_semaine)->format('Y-m-d');

                    if( $cible_date > $date_current->format('Y-m-d') ) {
                        $detail_dates[] = $cible_date;
                    }
                }
            }

            for($day = 0; $day < 7; $day++) {
                $date_debut_semaine = new Date(strtotime($date_semaine['anne'].'W'.sprintf("%02d", $date_semaine['semaine'])));
                $date_active = $date_debut_semaine->addDay($day);

                $active = 0;

                if( in_array($date_active->format('Y-m-d'), $detail_dates)) {
                    $active = 1;
                }
                $input_journee_commande_activier[$z][$date_active->format('Y-m-d')] = $active;
            }


        }

        foreach ($date['schedule'] as $date_semaine) {

            $z = $date_semaine['semaine'];

            for($day = 0; $day < 7; $day++) {
                $date_debut_semaine = new Date(strtotime($date_semaine['anne'].'W'.sprintf("%02d", $date_semaine['semaine'])));
                $date_active = $date_debut_semaine->addDay($day);

                $jours_semaine[$z][$day] = $date_active;

                $data_journee_by_semaine[$z][$date_active->format('Y-m-d')] = array();
            }


            $previsions_semaine_all = Prevision::where('code_client', '=', Auth::user()->code_client)
                ->where('date_semaine', '=', $date_semaine['semaine'] )
                ->where('date_year', '=', $date_semaine['anne'] )
                ->get();

            $previsions_semaine = $previsions_semaine_all->unique('article_id');

            if( count($previsions_semaine) > 0 ) {
                $data_confirmation[$z] = 1;
            } else {
                $data_confirmation[$z] = 0;
            }

            foreach ($previsions_semaine as $prevision) {
                $previsions[ $prevision->article_id ][ $date_semaine['semaine'] ] = $prevision->nombre_palettes;

                if( !array_key_exists($z, $data_previsions_total) ) {

                    $data_previsions_total[$z] = array(
                        'nombre_palettes' => $prevision->nombre_palettes,
                        'quantittes' => $prevision->nombre_palettes * $prevision->conditionnement
                    );

                } else {
                    $total_nombre_palettes = $data_previsions_total[$z]['nombre_palettes'] + $prevision->nombre_palettes ;
                    $total_quantittes = $data_previsions_total[$z]['quantittes'] + ( $prevision->nombre_palettes * $prevision->conditionnement );

                    $data_previsions_total[$z] = array(
                        'nombre_palettes' => $total_nombre_palettes,
                        'quantittes' => $total_quantittes
                    );


                }
            }

        }

        $data_commande_confirmer = array();
        $total_nombre_palettes_by_semaine =array();

        foreach ($data_journee_by_semaine as $data_numero_semaine => $journees_semaine) {

            $total_nombre_palettes_by_semaine[$data_numero_semaine] = 0;

            foreach ($journees_semaine as $data_jour_semain => $day) {
                $data_journee_by_semaine[$data_numero_semaine][$data_jour_semain] = 0;

                foreach ($articles_actuels as $article_actuel) {

                    $id_article = $article_actuel->article_id;
                    $prevision_commande_detail = Commande::where('code_client', '=', Auth::user()->code_client)
                        ->where('article_id', '=', $id_article )
                        ->where('date_commande', '=', $data_jour_semain )
                        ->first();

                    if($prevision_commande_detail) {
                        $data_journee_commande_prevision[$id_article][$data_jour_semain] = $prevision_commande_detail->nombre_palettes;
                        $data_commande_confirmer[$id_article][$data_jour_semain] = $prevision_commande_detail->confirmation;

                        $total_nombre_palettes_by_semaine[$data_numero_semaine] += $prevision_commande_detail->nombre_palettes;

                    } else {
                        $data_journee_commande_prevision[$id_article][$data_jour_semain] = 0;
                        $data_commande_confirmer[$id_article][$data_jour_semain] = 0;
                    }
                }
            }






        }

/*
        echo '<pre>';
        print_r($input_journee_commande_activier);
        echo '</pre>';
        exit();
        */



        return view('previsions2', compact('articles_actuels', 'date_month', 'date_last_update','date', 'nombre_mois', 'previsions', 'data_previsions_total', 'activation_update', 'data_confirmation', 'jours_semaine', 'data_journee_commande_prevision', 'input_journee_commande_activier', 'data_commande_confirmer', 'total_nombre_palettes_by_semaine', 'anne') );



    }

    public function getWeeks($today = null, $nombre_mois, $scheduleMonths = 1) {

        $today = !is_null($today) ? Carbon::createFromFormat('Y-m-d',$today) : Carbon::now();

        $startDate = Carbon::instance($today)->startOfMonth()->startOfWeek(); //  ->subDay() start on Sunday

        $endDate = Carbon::instance($startDate)->addMonths($scheduleMonths)->endOfMonth();

        $endDate->addDays(7 - $endDate->dayOfWeek);

        $epoch = Carbon::createFromTimestamp(0);
        $firstDay = $epoch->diffInDays($startDate);
        $lastDay = $epoch->diffInDays($endDate);



        $week=0;
        $monthNum = $today->month;
        $yearNum = $today->year;
        $prevDay = null;
        $theDay = $startDate;
        $prevMonth = $monthNum;

        $data = array();

        while ($firstDay < $lastDay) {

            if (($theDay->dayOfWeek == Carbon::SUNDAY) && (($theDay->month > $monthNum) || ($theDay->month == 1))) $monthNum = $theDay->month;
            if ($prevMonth > $monthNum) $yearNum++;

            $theMonth = Carbon::createFromFormat("Y-m-d",$yearNum."-".$monthNum."-01")->format('n');

            if (!array_key_exists($theMonth,$data)) {
                $data[$theMonth] = array();
            }


            if ($theDay->dayOfWeek == Carbon::MONDAY) {
                $data[$theMonth][$week]['anne'] = $theDay->year;
                $data[$theMonth][$week]['semaine'] = $theDay->weekOfYear;
                $data[$theMonth][$week]['debut_semaine'] = $theDay;
                $data[$theMonth][$week]['fin_semaine'] = $theDay->copy()->addDay(6);
                $week++;
            }

            $firstDay++;
            $theDay = $theDay->copy()->addDay();
            $prevMonth = $monthNum;
        }


        $nombre_week = count($data[$nombre_mois]);


        return array(
            'startDate' => $startDate,
            'endDate' => $data[$nombre_mois][$nombre_week-1]['fin_semaine'],
            'totalWeeks' => $nombre_week,
            'schedule' => $data[$nombre_mois],
        );

    }



    public function store(Request $request) {

        $nombre_mois = $request->input('nombre_mois');
        $anne_cible = $request->input('anne_cible');

        $date_month_carbon = Carbon::create($anne_cible, $nombre_mois, 1, 0, 0, 0);


        $date = $this->getWeeks( $date_month_carbon->toDateString(), $nombre_mois, 1 );



        Date::setLocale('fr');

        $date_month = new Date($date_month_carbon);



        $articles_actuels = ArticlesActuels::where('code_client', Auth::user()->code_client)->get();

        //$date_commande = $request->input('date_commande');

        $trace_detail=array();
        foreach ($articles_actuels as $article_actuel) {

            foreach ($date['schedule'] as $date_semaine) {

                $nombre_palettes = $request->input('quantitee_article_' . $article_actuel->article_id . '_' . $date_semaine['anne'] .'_' . $date_semaine['semaine']);


                $prevision_old = Prevision::where('code_client', $article_actuel->code_client)
                    ->where('article_id', $article_actuel->article_id)
                    //->where('conditionnement', $article_actuel->conditionnement)
                    //->where('stock_actuel_boraplast', $article_actuel->stock_actuel_boraplast)
                    ->where('date_semaine', $date_semaine['semaine'])
                    ->where('date_year', $date_semaine['anne'])
                    ->first();

                $prevision_action = Prevision::updateOrCreate(
                    [
                        'code_client' => $article_actuel->code_client,
                        'article_id' => $article_actuel->article_id,
                        'date_semaine' => $date_semaine['semaine'],
                        'date_year' => $date_semaine['anne']
                    ],
                    [
                        'conditionnement' => $article_actuel->conditionnement,
                        'stock_actuel_boraplast' => $article_actuel->stock_actuel_boraplast,
                        'nombre_palettes' => $nombre_palettes
                    ]
                );

                if($prevision_old) {
                    if($prevision_old->nombre_palettes != $prevision_action->nombre_palettes) {
                        $operation_detail['status'] = 'changement';

                        $operation_detail['article_id'] = $prevision_action->article_id;
                        $operation_detail['conditionnement'] = $prevision_action->conditionnement;
                        $operation_detail['stock_actuel_boraplast'] = $prevision_action->stock_actuel_boraplast;
                        $operation_detail['date_semaine'] = $prevision_action->date_semaine;
                        $operation_detail['date_year'] = $prevision_action->date_year;
                        $operation_detail['old_nombre_palettes'] = $prevision_old->nombre_palettes;
                        $operation_detail['new_nombre_palettes'] = $prevision_action->nombre_palettes;
                        $trace_detail[] = $operation_detail;
                    }

                } else {
                    $operation_detail['status'] = 'insertion';
                    $operation_detail['article_id'] = $prevision_action->article_id;
                    $operation_detail['conditionnement'] = $prevision_action->conditionnement;
                    $operation_detail['stock_actuel_boraplast'] = $prevision_action->stock_actuel_boraplast;
                    $operation_detail['date_semaine'] = $prevision_action->date_semaine;
                    $operation_detail['date_year'] = $prevision_action->date_year;
                    $operation_detail['new_nombre_palettes'] = $prevision_action->nombre_palettes;
                    $trace_detail[] = $operation_detail;
                }

            }

        }



        /****** parti traces ******/

        $trace = new Trace();
        $trace->user_id = Auth::id();

        $options_operation['type_operation'] = 'previsions_mensuelles';
        $options_operation['mois'] = $date_month->format('F Y');
        if(count($trace_detail)>0) {
            $options_operation['details'] = $trace_detail;
        } else {
            $options_operation['details'] = null;
        }

        $trace->operation = $options_operation;
        $trace->save();


        /****** ./parti traces ******/





        $client = Auth::user()->name;
        if( strlen(Auth::user()->nom_societe) > 0  ) {
            $client .=  ' ( ' . Auth::user()->nom_societe . ' )';
        }
        $users = User::role('Admin')->get();
        $emails_admin = '';
        foreach($users as $user) {
            if(strlen($emails_admin)>0) {
                $emails_admin = $emails_admin . ',' . $user->email;
            } else {
                $emails_admin = $user->email;
            }

        }
        $emails_admin .= ',' . Auth::user()->email;

        $data_mail = ['client' => $client, 'date_mois' => $date_month->format('F Y'), 'emails_admin' => $emails_admin];


        Mail::send('emails.prevision', $data_mail, function ($message) use ($data_mail)
        {

            $message->subject('Commande ' . $data_mail['date_mois']);

            $message->from('commercial@boraplast.com', 'BOARAPLAST');

            $message->to(explode(',', $data_mail['emails_admin']));

        });





        $response = array(
            'status' => 'success'
        );

        return response()->json($response);
    }

    public function store2(Request $request) {

        $nombre_semaine = $request->input('nombre_semaine');
        $nombre_anne = $request->input('nombre_anne');

        /***
         *
         *
         */
        Date::setLocale('fr');
        $date_month = new Date(strtotime($nombre_anne.'W'.sprintf("%02d", $nombre_semaine)));

        $articles_actuels = ArticlesActuels::where('code_client', Auth::user()->code_client)->get();

        $trace_detail=array();
        foreach ($articles_actuels as $article_actuel) {

            $nombre_palettes = $request->input('quantitee_article_' . $article_actuel->article_id . '_' . $nombre_anne .'_' . $nombre_semaine);


            $prevision_old = Prevision::where('code_client', $article_actuel->code_client)
                ->where('article_id', $article_actuel->article_id)
                //->where('conditionnement', $article_actuel->conditionnement)
                //->where('stock_actuel_boraplast', $article_actuel->stock_actuel_boraplast)
                ->where('date_semaine', $nombre_semaine)
                ->where('date_year', $nombre_anne)
                ->first();

            $prevision_action = Prevision::updateOrCreate(
                [
                    'code_client' => $article_actuel->code_client,
                    'article_id' => $article_actuel->article_id,
                    'date_semaine' => $nombre_semaine,
                    'date_year' => $nombre_anne
                ],
                [
                    'conditionnement' => $article_actuel->conditionnement,
                    'stock_actuel_boraplast' => $article_actuel->stock_actuel_boraplast,
                    'nombre_palettes' => $nombre_palettes
                ]
            );

            if($prevision_old) {
                if($prevision_old->nombre_palettes != $prevision_action->nombre_palettes) {
                    $operation_detail['status'] = 'changement';

                    $operation_detail['article_id'] = $prevision_action->article_id;
                    $operation_detail['conditionnement'] = $prevision_action->conditionnement;
                    $operation_detail['stock_actuel_boraplast'] = $prevision_action->stock_actuel_boraplast;
                    $operation_detail['date_semaine'] = $prevision_action->date_semaine;
                    $operation_detail['date_year'] = $prevision_action->date_year;
                    $operation_detail['old_nombre_palettes'] = $prevision_old->nombre_palettes;
                    $operation_detail['new_nombre_palettes'] = $prevision_action->nombre_palettes;
                    $trace_detail[] = $operation_detail;
                }

            } else {
                $operation_detail['status'] = 'insertion';
                $operation_detail['article_id'] = $prevision_action->article_id;
                $operation_detail['conditionnement'] = $prevision_action->conditionnement;
                $operation_detail['stock_actuel_boraplast'] = $prevision_action->stock_actuel_boraplast;
                $operation_detail['date_semaine'] = $prevision_action->date_semaine;
                $operation_detail['date_year'] = $prevision_action->date_year;
                $operation_detail['new_nombre_palettes'] = $prevision_action->nombre_palettes;
                $trace_detail[] = $operation_detail;
            }

        }

        /****** parti traces ******/

        $trace = new Trace();
        $trace->user_id = Auth::id();

        $options_operation['type_operation'] = 'previsions_mensuelles';
        $options_operation['mois'] = $date_month->format('F Y');
        if(count($trace_detail)>0) {
            $options_operation['details'] = $trace_detail;
        } else {
            $options_operation['details'] = null;
        }

        $trace->operation = $options_operation;
        $trace->save();


        /****** ./parti traces ******/

        $client = Auth::user()->name;
        if( strlen(Auth::user()->nom_societe) > 0  ) {
            $client .=  ' ( ' . Auth::user()->nom_societe . ' )';
        }
        $users = User::role('Admin')->get();
        $emails_admin = '';
        foreach($users as $user) {
            if(strlen($emails_admin)>0) {
                $emails_admin = $emails_admin . ',' . $user->email;
            } else {
                $emails_admin = $user->email;
            }

        }
        $emails_admin .= ',' . Auth::user()->email;

        $data_mail = ['client' => $client, 'date_mois' => $date_month->format('F Y') . ' / S' . $date_month->format('W'), 'emails_admin' => $emails_admin];


        Mail::send('emails.prevision', $data_mail, function ($message) use ($data_mail)
        {

            $message->subject('Commande ' . $data_mail['date_mois']);

            $message->from('commercial@boraplast.com', 'BOARAPLAST');

            $message->to(explode(',', $data_mail['emails_admin']));

        });


        $response = array(
            'status' => 'success'
        );

        return response()->json($response);

    }


    public function saveRepartition(Request $request) {

        $code_client = Auth::user()->code_client;
        $article_id = $request->input('id_article');

        $trace_detail=array();
        for($jour=1; $jour <= 7; $jour++) {
            $date_commande = $request->input('date_day_'.$jour);
            $nombre_palettes = $request->input('nb_palettes_day_'.$jour);0;


            $previsionc_old = Commande::where('code_client', $code_client)
                ->where('article_id', $article_id)
                ->where('date_commande', $date_commande)
                ->first();

            $previsionc = Commande::updateOrCreate(
                [
                    'code_client' => $code_client,
                    'article_id' => $article_id,
                    'date_commande' => $date_commande
                ],
                ['nombre_palettes' => $nombre_palettes]
            );

            if($previsionc_old) {
                if($previsionc_old->nombre_palettes != $previsionc->nombre_palettes) {
                    $operation_detail['status'] = 'changement';

                    $operation_detail['article_id'] = $previsionc->article_id;
                    $operation_detail['date_commande'] = $previsionc->date_commande;
                    $operation_detail['old_nombre_palettes'] = $previsionc_old->nombre_palettes;
                    $operation_detail['new_nombre_palettes'] = $previsionc->nombre_palettes;
                    $trace_detail[] = $operation_detail;
                }

            } else {
                $operation_detail['status'] = 'insertion';
                $operation_detail['article_id'] = $previsionc->article_id;
                $operation_detail['date_commande'] = $previsionc->date_commande;
                $operation_detail['new_nombre_palettes'] = $previsionc->nombre_palettes;
                $trace_detail[] = $operation_detail;
            }
        }

        /****** parti traces ******/

        $trace = new Trace();
        $trace->user_id = Auth::id();

        $options_operation['type_operation'] = 'repartition_hebdomadaire';
        $options_operation['article'] = $article_id;
        if(count($trace_detail)>0) {
            $options_operation['details'] = $trace_detail;
        } else {
            $options_operation['details'] = null;
        }

        $trace->operation = $options_operation;
        $trace->save();


        /****** ./parti traces ******/





        $response = array(
            'status' => 'success'
        );

        return response()->json($response);
    }




}