<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use Auth;

//Importing laravel-permission models
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

//Enables us to output flash messaging
use Session;

class UserController extends Controller
{

    public function __construct() {
        $this->middleware(['auth', 'isAdmin']); //isAdmin middleware lets only users with a //specific permission permission to access these resources
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return view('users.index')->with('users', $users);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Get all roles and pass it to the view
        $roles = Role::get();

        $days = array('lundi','mardi','mercredi','jeudi','vendredi','samedi','dimanche');

        return view('users.create', ['roles'=>$roles, 'days'=> $days]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validate name, email and password fields
        $this->validate($request, [
            'name'=>'required|max:120',
            'email'=>'required|email|unique:users',
            'password'=>'required|min:6|confirmed'
        ]);


        $days_commande = $request->input('days_commande');

        $user = new User;
        $user->email = $request->email;
        $user->name = $request->name;
        $user->password = $request->password;
        $user->code_client = $request->code_client;
        $user->nom_societe = $request->nom_societe;
        $user->jour_commande = $request->jour_commande;


        if(isset($request->days_commande)) {
            $days_commande = $request->input('days_commande');
            if( count($days_commande) > 0 ) {
                $user->days_commande = implode('#', $days_commande);
            } else {
                $user->days_commande = '';
            }
        } else  {
            $user->days_commande = '';
        }

        $user->save();

        //$user = User::create($request->only('email', 'name', 'password', 'code_client', 'nom_societe', 'jour_commande', 'days_commande')); //Retrieving only the email and password data

        $roles = $request['roles']; //Retrieving the roles field
        //Checking if a role was selected
        if (isset($roles)) {

            foreach ($roles as $role) {
                $role_r = Role::where('id', '=', $role)->firstOrFail();
                $user->assignRole($role_r); //Assigning role to user
            }
        }
        //Redirect to the users.index view and display message
        return redirect()->route('users.index')
            ->with('flash_message',
                'User successfully added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect('users');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id); //Get user with specified id
        $roles = Role::get(); //Get all roles

        $days = array('lundi','mardi','mercredi','jeudi','vendredi','samedi','dimanche');

        $user->days_commande = explode("#", $user->days_commande);

        return view('users.edit', compact('user', 'roles', 'days')); //pass user and roles data to view
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id); //Get role specified by id

        //Validate name, email and password fields
        $this->validate($request, [
            'name'=>'required|max:120',
            'email'=>'required|email|unique:users,email,'.$id
        ]);
        /*
         * ,
            'password'=>'required|min:6|confirmed'
        */

        $user->email = $request->email;
        $user->name = $request->name;
        $user->code_client = $request->code_client;
        $user->nom_societe = $request->nom_societe;
        $user->jour_commande = $request->jour_commande;

        if(isset($request->days_commande)) {
            $days_commande = $request->input('days_commande');
            if( count($days_commande) > 0 ) {
                $user->days_commande = implode('#', $days_commande);
            } else {
                $user->days_commande = '';
            }
        } else  {
            $user->days_commande = '';
        }


        $user->save();

        //$input = $request->only(['name', 'email', 'code_client', 'nom_societe', 'jour_commande']);
        //$user->fill($input)->save();
        $roles = $request['roles']; //Retreive all roles


        if (isset($roles)) {
            $user->roles()->sync($roles);  //If one or more role is selected associate user to roles
        }
        else {
            $user->roles()->detach(); //If no role is selected remove exisiting role associated to a user
        }
        return redirect()->route('users.index')
            ->with('flash_message',
                'User successfully edited.');
    }

    public function updatePass(Request $request, $id)
    {
        $user = User::findOrFail($id); //Get role specified by id

        //Validate name, email and password fields
        $this->validate($request, [
            'password'=>'required|min:6|confirmed'
        ]);
        /*
         * ,

        */
        $input = $request->only(['password']); //Retreive the name, email and password fields //,
        $user->fill($input)->save();

        return redirect()->route('users.index')
            ->with('flash_message',
                'Pass successfully chnaged.');
    }

    public function updateLogo(Request $request, $id)
    {

        $user = User::findOrFail($id); //Get role specified by id

        if ($request->hasFile('logo_societe')) {
            //

            //$path = $request->logo_societe->store('images');

            $extension = $request->logo_societe->extension();

            $file_name = $user->id . '_' .date('Ymd_His') . '.' . $extension;

            $request->logo_societe->move(public_path('images'), $file_name );

            $user->logo_societe = 'images/' . $file_name;
            $user->save();

            return redirect()->route('users.index')
                ->with('flash_message',
                    'Photo successfully chnaged.');
        }

        return redirect()->route('users.index')
            ->with('flash_error',
                'Not file.');




    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Find a user with a given id and delete
        $user = User::findOrFail($id);
        $user->delete();

        return redirect()->route('users.index')
            ->with('flash_message',
                'User successfully deleted.');
    }
}
