<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Prevision;
use App\Commande;

use Date;

use Carbon\Carbon;

class ErpController extends Controller
{
    protected $last_update = null;

    public function getCommandes($last_update)
    {

        $data_commandes = array();

        $this->getDate($last_update);
        if($this->last_update) {

            $commandes = Commande::where('updated_at', '>' , $this->last_update)
                ->where('confirmation', 1)
                ->orderBy('updated_at', 'asc')
                ->get();



            foreach ($commandes as $commande) {
                $date_last_update = Carbon::createFromFormat('Y-m-d H:i:s',$commande->updated_at);
                $data_commandes[] = array(
                    'customer' => $commande->code_client,
                    'item' => $commande->article->reference,
                    'date_order' => $commande->date_commande,
                    'quantity_palettes' => $commande->nombre_palettes,
                    'packaging' => $commande->conditionnement,
                    'quantity' => $commande->nombre_palettes * $commande->conditionnement,
                    'unit' => $commande->article->unite,
                    'ordred' => $commande->id,
                    'updated' => $date_last_update->format('YmdHis')
                );


            }

        } else {
            $data_commandes['status'] = 'erreur format date';
        }

        return response()->json($data_commandes);

    }

    public function getPrevisions($last_update)
    {
        $this->getDate($last_update);

        $data_commandes = array();

        if($this->last_update) {

            $previsions = Prevision::where('updated_at', '>' , $this->last_update)
                ->orderBy('updated_at', 'asc')
                ->get();

            foreach ($previsions as $prevision) {


                $date_prevision = new Date(strtotime($prevision->date_year.'W'.sprintf("%02d", $prevision->date_semaine)));

                $date_last_update = Carbon::createFromFormat('Y-m-d H:i:s',$prevision->updated_at);


                $data_commandes[] = array(
                    'customer' => $prevision->code_client,
                    'item' => $prevision->article->reference,
                    'week' => $prevision->date_semaine,
                    'month' => intval($date_prevision->format('n')),
                    'year' => $prevision->date_year,
                    'quantity_palettes' => $prevision->nombre_palettes,
                    'packaging' => $prevision->conditionnement,
                    'quantity' => $prevision->nombre_palettes * $prevision->conditionnement,
                    'unit' => $prevision->article->unite,
                    'ordred' => $prevision->id,
                    'updated' => $date_last_update->format('YmdHis')
                );


            }

        } else {
            $data_commandes['status'] = 'erreur format date';
        }


        return response()->json($data_commandes);

    }

    public function getDate($format_numerique) {

        $Y = substr ( $format_numerique , 0, 4 );
        $m = substr ( $format_numerique , 4, 2 );
        $d = substr ( $format_numerique , 6, 2 );

        $H = substr ( $format_numerique , 8, 2 );
        $i = substr ( $format_numerique , 10, 2 );
        $s = substr ( $format_numerique , 12, 2 );


        if( strlen($format_numerique)==14 && checkdate( $m , $d , $Y ) && $this->checktime($H, $i, $s) ) {
            $this->last_update = $Y.'-'.$m.'-'.$d.' '.$H.':'.$i.':'.$s;
        } else {
            $this->last_update = null;
        }



    }

    public function checktime($hour, $min, $sec) {
        if ($hour < 0 || $hour > 23 || !is_numeric($hour)) {
            return false;
        }
        if ($min < 0 || $min > 59 || !is_numeric($min)) {
            return false;
        }
        if ($sec < 0 || $sec > 59 || !is_numeric($sec)) {
            return false;
        }
        return true;
    }
}
