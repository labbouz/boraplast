<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Session;

use App\Article;
use App\ArticlesActuels;
use App\Commande;
use App\Prevision;
use App\Previsionc;
use Mail;

use Date;

use Carbon\Carbon;

use App\User;

use App\Trace;

class CommandesController extends Controller
{
    public function __construct() {
        $this->middleware(['auth', 'clearance']);
    }

    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function semaine($nombre_semaine=null, $nombre_anne=null)
    {
        $week = (int) date('W');
        if(!$nombre_anne || !$nombre_semaine || $nombre_semaine > ($week+1) ) {
            $nombre_anne = date('Y');

            return redirect()->route('commandes', ['nombre_semaine' => $week, 'nombre_anne' => $nombre_anne] );
        }

        Date::setLocale('fr');

        $articles_actuels = ArticlesActuels::where('code_client', Auth::user()->code_client)->get();

        foreach ($articles_actuels as $article_actuel) {
            $article_actuel->reference=$article_actuel->article->reference;
            $article_actuel->designation=$article_actuel->article->designation;
            $article_actuel->unite=$article_actuel->article->unite;

        }

        $articles_actuels->sortBy("reference");

        $date = new Date(strtotime($nombre_anne.'W'.sprintf("%02d", $nombre_semaine)));


        $days_commande = Auth::user()->days_commande;

        if(strlen($days_commande)>0) {


            $days_semaines = array('lundi','mardi','mercredi','jeudi','vendredi','samedi','dimanche');

            $jours_disponible = explode('#', $days_commande);


            $detail_dates = array();

            $date_current = Date::now();

            foreach ($jours_disponible as $jour_disponible) {
                $key_data_semaine = array_search($jour_disponible,$days_semaines);

                $date_temporaire = new Date(strtotime($nombre_anne.'W'.sprintf("%02d", $nombre_semaine)));

                $cible_date = $date_temporaire->addDay($key_data_semaine)->format('Y-m-d');

                if( $cible_date > $date_current->format('Y-m-d') ) {
                    $detail_dates[] = $cible_date;
                }
            }

        } else {
            $jours_disponible = array();


            $date_current = Date::now();
            $jour_n = Auth::user()->jour_commande;

            for($jour_inex=0; $jour_inex<$jour_n ;$jour_inex++) {
                $detail_dates[] = $date_current->addDay(1)->format('Y-m-d');
            }


        }

        if(count($articles_actuels)) {
            $first_article = $articles_actuels->first();
            $date_last_update = Carbon::createFromFormat('Y-m-d H:i:s',$first_article->updated_at);
        } else {
            $date_last_update = new Carbon();
        }


        if( auth()->check() && !auth()->user()->hasRole('Client 2') ) {
            for($day = 0; $day < 7; $day++) {
                $date_debut_semaine = new Date(strtotime($nombre_anne.'W'.sprintf("%02d", $nombre_semaine)));
                $date_active = $date_debut_semaine->addDay($day);

                $active = 0;

                $count_number_commandes = Commande::where('date_commande', $date_active->format('Y-m-d'))->where('code_client', Auth::user()->code_client)->count();

                if( in_array($date_active->format('Y-m-d'), $detail_dates) && $count_number_commandes == 0) {
                    $active = 1;
                }

                $date_semaine[$day]['active'] = $active;
                $date_semaine[$day]['date_active'] = $date_active;
            }

            $le_jour_commande_active = $this->nextCommandeActive($nombre_semaine, $nombre_anne, $date_semaine);

            $dernier_date_commande_json = Commande::select('date_commande')
                ->where('code_client', Auth::user()->code_client)
                ->where('confirmation', 1)
                ->orderBy('date_commande', 'desc')->first();

            if($dernier_date_commande_json) {
                $dernier_date_commande = $dernier_date_commande_json->date_commande;
            } else {
                $dernier_date_commande = date('Y-m-d');
            }


            return view('commandes', compact('nombre_semaine', 'date', 'date_semaine', 'jours_disponible', 'articles_actuels', 'date_last_update', 'le_jour_commande_active', 'dernier_date_commande')); //
        } else{

            for($day = 0; $day < 7; $day++) {
                $date_debut_semaine = new Date(strtotime($nombre_anne.'W'.sprintf("%02d", $nombre_semaine)));
                $date_active = $date_debut_semaine->addDay($day);

                $active = 0;

                $count_number_commandes = Commande::where('date_commande', $date_active->format('Y-m-d'))
                    ->where('code_client', Auth::user()->code_client)
                    ->where('confirmation', 1)
                    ->count();

                if( in_array($date_active->format('Y-m-d'), $detail_dates) && $count_number_commandes == 0) {
                    $active = 1;
                }

                $date_semaine[$day]['active'] = $active;
                $date_semaine[$day]['date_active'] = $date_active;
            }



            // pour calculer les chiffre prevision
            $previsions_semaine = Prevision::where('code_client', '=', Auth::user()->code_client)
                ->where('date_semaine', '=', $nombre_semaine )
                ->where('date_year', '=', $nombre_anne )
                ->get();

            $data_previsions_total = array(
                'nombre_palettes' => 0,
                'quantittes' => 0
            );

            $data_previsions_c = array();

            foreach ($previsions_semaine as $prevision) {
                $previsions[ $prevision->article_id ] = $prevision->nombre_palettes;

                $total_nombre_palettes = $data_previsions_total['nombre_palettes'] + $prevision->nombre_palettes ;
                $total_quantittes = $data_previsions_total['quantittes'] + ( $prevision->nombre_palettes * $prevision->conditionnement );

                $data_previsions_total = array(
                    'nombre_palettes' => $total_nombre_palettes,
                    'quantittes' => $total_quantittes
                );
            }

            $data_total_commande = array();
            for($day = 0; $day < 7; $day++) {
                $date_debut_semaine = new Date(strtotime($nombre_anne.'W'.sprintf("%02d", $nombre_semaine)));
                $date_active = $date_debut_semaine->addDay($day);

                $total_commande=0;
                $total_quantite=0;

                foreach ($articles_actuels as $article_actuel) {

                    $id_article = $article_actuel->article_id;
                    $prevision_commande_detail = Commande::where('code_client', '=', Auth::user()->code_client)
                        ->where('article_id', '=', $id_article )
                        ->where('date_commande', '=', $date_active->format('Y-m-d') )
                        ->first();

                    if($prevision_commande_detail) {
                        $data_previsions_c[$id_article][$date_active->format('Y-m-d')] = $prevision_commande_detail->nombre_palettes;
                        $total_commande += $prevision_commande_detail->nombre_palettes;
                        $total_quantite += $prevision_commande_detail->nombre_palettes * $article_actuel->conditionnement;
                    } else {
                        $data_previsions_c[$id_article][$date_active->format('Y-m-d')] = 0;
                    }
                }

                $data_total_commande[$date_active->format('Y-m-d')]['np'] = $total_commande;
                $data_total_commande[$date_active->format('Y-m-d')]['qt'] = $total_quantite;
            }

            /*
            echo '<pre>';
            print_r($data_previsions_c);
            echo '</pre>';
            exit();
            */

            $le_jour_commande_active = $this->nextCommandeActive($nombre_semaine, $nombre_anne, $date_semaine);

            $dernier_date_commande_json = Commande::select('date_commande')
                ->where('code_client', Auth::user()->code_client)
                ->where('confirmation', 1)
                ->orderBy('date_commande', 'desc')->first();

            if($dernier_date_commande_json) {
                $dernier_date_commande = $dernier_date_commande_json->date_commande;
            } else {
                $dernier_date_commande = date('Y-m-d');
            }

            return view('commandes2', compact('nombre_semaine', 'date', 'date_semaine', 'jours_disponible', 'articles_actuels', 'date_last_update', 'previsions', 'data_previsions_total', 'data_previsions_c', 'data_total_commande', 'le_jour_commande_active', 'dernier_date_commande')); //
        }

    }

    public function nextCommandeActive($nombre_semaine, $nombre_anne, $date_semaine) {

        $dernier_date_commande = Commande::select('date_commande')
            ->where('code_client', Auth::user()->code_client)
            ->where('confirmation', 1)
            ->orderBy('date_commande', 'desc')->first();

        if($dernier_date_commande) {

            $date_semaine_dernier_commande = new Date($dernier_date_commande->date_commande);

            $semaine_commande = $date_semaine_dernier_commande->format('W');
        } else {
            $semaine_commande = date('W');
        }

        if( $nombre_semaine == $semaine_commande) {
            foreach ($date_semaine as $date_semaine_detail) {

                if($date_semaine_detail['active'] ) {
                    return $date_semaine_detail['date_active']->format('Y-m-d');
                }

            }
        } else {

            if(!$this->isExistCommandeActive($semaine_commande, $nombre_anne)) {

                foreach ($date_semaine as $date_semaine_detail) {

                    if($date_semaine_detail['active'] ) {
                        return $date_semaine_detail['date_active']->format('Y-m-d');
                    }

                }
            }

        }

        return null;

    }

    public function isExistCommandeActive($semaine_commande, $nombre_anne) {

        $table_a_verfier = $this->getDaysActive($semaine_commande, $nombre_anne);

        foreach ($table_a_verfier as $date_semaine_detail) {

            if($date_semaine_detail['active'] ) {
                return true;
            }

        }

        return false;

    }

    public function getDaysActive($nombre_semaine, $nombre_anne) {
        $days_commande = Auth::user()->days_commande;

        $detail_dates = array();

        $date_semaine = array();

        if(strlen($days_commande)>0) {
            $days_semaines = array('lundi','mardi','mercredi','jeudi','vendredi','samedi','dimanche');
            $jours_disponible = explode('#', $days_commande);
            $date_current = Date::now();

            foreach ($jours_disponible as $jour_disponible) {
                $key_data_semaine = array_search($jour_disponible,$days_semaines);
                $date_temporaire = new Date(strtotime($nombre_anne.'W'.sprintf("%02d", $nombre_semaine)));
                $cible_date = $date_temporaire->addDay($key_data_semaine)->format('Y-m-d');
                if( $cible_date > $date_current->format('Y-m-d') ) {
                    $detail_dates[] = $cible_date;
                }
            }

        } else {
            $date_current = Date::now();
            $jour_n = Auth::user()->jour_commande;

            for($jour_inex=0; $jour_inex<$jour_n ;$jour_inex++) {
                $detail_dates[] = $date_current->addDay(1)->format('Y-m-d');
            }

        }

        if( auth()->check() && !auth()->user()->hasRole('Client 2') ) {
            for($day = 0; $day < 7; $day++) {
                $date_debut_semaine = new Date(strtotime($nombre_anne.'W'.sprintf("%02d", $nombre_semaine)));
                $date_active = $date_debut_semaine->addDay($day);

                $active = 0;

                $count_number_commandes = Commande::where('date_commande', $date_active->format('Y-m-d'))->where('code_client', Auth::user()->code_client)->count();

                if( in_array($date_active->format('Y-m-d'), $detail_dates) && $count_number_commandes == 0) {
                    $active = 1;
                }

                $date_semaine[$day]['active'] = $active;
                $date_semaine[$day]['date_active'] = $date_active;
            }

        } else {

            for ($day = 0; $day < 7; $day++) {
                $date_debut_semaine = new Date(strtotime($nombre_anne . 'W' . sprintf("%02d", $nombre_semaine)));
                $date_active = $date_debut_semaine->addDay($day);

                $active = 0;

                $count_number_commandes = Commande::where('date_commande', $date_active->format('Y-m-d'))
                    ->where('code_client', Auth::user()->code_client)
                    ->where('confirmation', 1)
                    ->count();

                if (in_array($date_active->format('Y-m-d'), $detail_dates) && $count_number_commandes == 0) {
                    $active = 1;
                }

                $date_semaine[$day]['active'] = $active;
                $date_semaine[$day]['date_active'] = $date_active;
            }
        }

        return $date_semaine;
    }

    public function historiqueSemaine($nombre_semaine=null)
    {
        $week = (int) date('W');
        if(!$nombre_semaine || $nombre_semaine > ($week+1) ) {
            return redirect()->route('historique.commandes', $week);
        }

        Date::setLocale('fr');

        $date_semaine_first = new Date(strtotime(date('Y').'W'.sprintf("%02d", $nombre_semaine)));

        $commandes = Commande::where('code_client', Auth::user()->code_client)
            ->where('date_commande', '>=', $date_semaine_first->format('Y-m-d') )
            ->where('date_commande', '<=', $date_semaine_first->addDay(6)->format('Y-m-d') )
            ->get();


        $data_articles = array();
        $data_commandes = array();

        $data_commandes_total = array();

        foreach ($commandes as $commande) {

            $id_article = $commande->article_id;

            if( !array_key_exists($id_article, $data_articles) ) {

                $data_articles[$id_article] = array(
                    'reference' => $commande->article->reference,
                    'designation' => $commande->article->designation
                );

            }

            $date_commande = new Date($commande->date_commande);
            $z = $date_commande->format('z');

            $data_commandes[$id_article][$z] = array(
                'conditionnement' => $commande->conditionnement,
                'stock_actuel_boraplast' => $commande->stock_actuel_boraplast,
                'nombre_palettes' => $commande->nombre_palettes,
                'quantittes' => number_format ($commande->nombre_palettes * $commande->conditionnement, 0, ',', ' ' ),
                'date_commande' => $commande->date_commande
            );

            if( !array_key_exists($z, $data_commandes_total) ) {

                $data_commandes_total[$z] = array(
                    'nombre_palettes' => $commande->nombre_palettes,
                    'quantittes' => $commande->nombre_palettes * $commande->conditionnement,
                    'date' => $date_commande->format('d-m-Y')
                );

            } else {
                $total_nombre_palettes = $data_commandes_total[$z]['nombre_palettes'] + $commande->nombre_palettes ;
                $total_quantittes = $data_commandes_total[$z]['quantittes'] + ( $commande->nombre_palettes * $commande->conditionnement );

                $data_commandes_total[$z] = array(
                    'nombre_palettes' => $total_nombre_palettes,
                    'quantittes' => $total_quantittes,
                    'date' => $date_commande->format('d-m-Y')
                );


            }
        }


        $commandes->sortBy("reference");

        $date = new Date(strtotime(date('Y').'W'.sprintf("%02d", $nombre_semaine)));


        for($day = 0; $day < 7; $day++) {
            $date_debut_semaine = new Date(strtotime(date('Y').'W'.sprintf("%02d", $nombre_semaine)));
            $date_active = $date_debut_semaine->addDay($day);

            $active = 0;

            $count_number_commandes = Commande::where('date_commande', $date_active->format('Y-m-d'))->where('code_client', Auth::user()->code_client)->count();

            if( $count_number_commandes > 0) {
                $active = 1;
            }


            $date_semaine[$day]['active'] = $active;
            $date_semaine[$day]['date_active'] = $date_active;
        }

        return view('historique_commandes', compact('nombre_semaine', 'date', 'date_semaine', 'data_articles', 'data_commandes', 'data_commandes_total')); //

    }

    public function store(Request $request) {

        $articles_actuels = ArticlesActuels::where('code_client', Auth::user()->code_client)->get();

        $date_commande = $request->input('date_commande');

        $trace_detail=array();
        foreach ($articles_actuels as $article_actuel) {

            $nombre_palettes = $request->input('quantitee_article_' . $article_actuel->id);

            $commande_adedd = new Commande;
            $commande_adedd->code_client = $article_actuel->code_client;
            $commande_adedd->article_id = $article_actuel->article_id;
            $commande_adedd->conditionnement = $article_actuel->conditionnement;
            $commande_adedd->stock_actuel_boraplast = $article_actuel->stock_actuel_boraplast;
            $commande_adedd->nombre_palettes = $nombre_palettes;
            $commande_adedd->date_commande = $date_commande;
            $commande_adedd->confirmation = 1;
            $commande_adedd->save();


            $operation_detail['status'] = 'insertion';
            $operation_detail['article_id'] = $commande_adedd->article_id;
            $operation_detail['conditionnement'] = $commande_adedd->conditionnement;
            $operation_detail['stock_actuel_boraplast'] = $commande_adedd->stock_actuel_boraplast;
            $operation_detail['date_commande'] = $commande_adedd->date_commande;
            $operation_detail['nombre_palettes'] = $commande_adedd->nombre_palettes;
            $trace_detail[] = $operation_detail;


        }

        /****** parti traces ******/

        $trace = new Trace();
        $trace->user_id = Auth::id();

        $options_operation['type_operation'] = 'commande';
        $options_operation['date_commande'] = $date_commande;
        if(count($trace_detail)>0) {
            $options_operation['details'] = $trace_detail;
        } else {
            $options_operation['details'] = null;
        }

        $trace->operation = $options_operation;
        $trace->save();


        /****** ./parti traces ******/

        $client = Auth::user()->name;
        if( strlen(Auth::user()->nom_societe) > 0  ) {
            $client .=  ' ( ' . Auth::user()->nom_societe . ' )';
        }

        $users = User::role('Admin')->get();
        $emails_admin = '';
        foreach($users as $user) {
            if(strlen($emails_admin)>0) {
                $emails_admin = $emails_admin . ',' . $user->email;
            } else {
                $emails_admin = $user->email;
            }

        }
        $emails_admin .= ',' . Auth::user()->email;

        $data_mail = ['client' => $client, 'date_commande' => $date_commande, 'emails_admin' => $emails_admin];

        Mail::send('emails.commande', $data_mail, function ($message) use ($data_mail)
        {

            $message->subject('Commande ' . $data_mail['date_commande']);

            $message->from('commercial@boraplast.com', 'BOARAPLAST');

            $message->to(explode(',', $data_mail['emails_admin']));

        });


        $response = array(
            'status' => 'success'
        );

        return response()->json($response);
    }

    public function store2(Request $request) {

        $articles_actuels = ArticlesActuels::where('code_client', Auth::user()->code_client)->get();

        $date_commande = $request->input('date_commande');

        $trace_detail=array();
        foreach ($articles_actuels as $article_actuel) {

            $nombre_palettes = $request->input('quantitee_article_' . $article_actuel->id);

            $commande_adedd = Commande::updateOrCreate(
                [
                    'code_client' => $article_actuel->code_client,
                    'article_id' => $article_actuel->article_id,
                    'date_commande' => $date_commande
                ],
                [
                    'nombre_palettes' => $nombre_palettes,
                    'confirmation' => 1,
                    'conditionnement' => $article_actuel->conditionnement,
                    'stock_actuel_boraplast' => $article_actuel->stock_actuel_boraplast
                ]
            );

            $operation_detail['status'] = 'insertion';
            $operation_detail['article_id'] = $commande_adedd->article_id;
            $operation_detail['conditionnement'] = $commande_adedd->conditionnement;
            $operation_detail['stock_actuel_boraplast'] = $commande_adedd->stock_actuel_boraplast;
            $operation_detail['date_commande'] = $commande_adedd->date_commande;
            $operation_detail['nombre_palettes'] = $commande_adedd->nombre_palettes;
            $trace_detail[] = $operation_detail;
        }

        /****** parti traces ******/

        $trace = new Trace();
        $trace->user_id = Auth::id();

        $options_operation['type_operation'] = 'commande';
        $options_operation['date_commande'] = $date_commande;
        if(count($trace_detail)>0) {
            $options_operation['details'] = $trace_detail;
        } else {
            $options_operation['details'] = null;
        }

        $trace->operation = $options_operation;
        $trace->save();


        /****** ./parti traces ******/

        $client = Auth::user()->name;
        if( strlen(Auth::user()->nom_societe) > 0  ) {
            $client .=  ' ( ' . Auth::user()->nom_societe . ' )';
        }

        $users = User::role('Admin')->get();
        $emails_admin = '';
        foreach($users as $user) {
            if(strlen($emails_admin)>0) {
                $emails_admin = $emails_admin . ',' . $user->email;
            } else {
                $emails_admin = $user->email;
            }

        }
        $emails_admin .= ',' . Auth::user()->email;

        $data_mail = ['client' => $client, 'date_commande' => $date_commande, 'emails_admin' => $emails_admin];

        Mail::send('emails.commande', $data_mail, function ($message) use ($data_mail)
        {

            $message->subject('Commande ' . $data_mail['date_commande']);

            $message->from('commercial@boraplast.com', 'BOARAPLAST');

            $message->to(explode(',', $data_mail['emails_admin']));

        });


        $response = array(
            'status' => 'success'
        );

        return response()->json($response);
    }


}