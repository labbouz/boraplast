<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Article;
use App\ArticlesActuels;

class getArticles extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'articles:get';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Récuper les articles from Navision';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $url = env('URL_NAVISION', 'http://www.boraplast.com/data-navision.php');

        $aricles = json_decode(file_get_contents($url), true);

        //Article::truncate();
        ArticlesActuels::truncate();

        $resultat = '';
        foreach ($aricles as $aricle) {

            $article_search = Article::where('reference', '=', $aricle['code_article'])->first();

            if($article_search) {
                $article_id = $article_search->id;
            } else {

                $article_cible = new Article;
                $article_cible->reference = $aricle['code_article'];
                $article_cible->reference = $aricle['code_article'];
                $article_cible->designation = $aricle['designation_article'];
                $article_cible->unite = 'UN';
                $article_cible->save();

                $article_id = $article_cible->id;

            }


            $article_actuel = new ArticlesActuels;
            $article_actuel->create([
                'code_client' => $aricle['code_client'],
                'article_id' => $article_id,
                'conditionnement' => $aricle['conditionnement'],
                'stock_actuel_boraplast' => $aricle['stock_actuel_boraplast']
            ]);


        }

        $this->info( count($aricles) . ' aricles imported from NAVISION');

    }
}
