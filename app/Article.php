<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $table = 'articles';

    protected $fillable = [
        'reference', 'designation', 'unite'
    ];


    public function articlesActuels()
    {
        return $this->hasMany('\App\ArticlesActuels', 'article_id');
    }

    public function commandes()
    {
        return $this->hasMany('\App\Commande', 'article_id');
    }

    public function previsions()
    {
        return $this->hasMany('\App\Prevision', 'article_id');
    }

    public function previsionsc()
    {
        return $this->hasMany('\App\Previsionc', 'article_id');
    }
}
