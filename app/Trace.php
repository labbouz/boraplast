<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trace extends Model
{
    protected $table = 'traces';

    protected $casts = [
        'operation' => 'array',
    ];

    protected $fillable = [
        'user_id', 'operation'
    ];


    public function user()
    {
        return $this->belongsTo('\App\User');
    }


}
