<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prevision extends Model
{
    protected $table = 'previsions';

    protected $fillable = [
        'code_client', 'article_id', 'conditionnement', 'stock_actuel_boraplast', 'nombre_palettes', 'date_semaine', 'date_year'
    ];


    public function article()
    {
        return $this->belongsTo('\App\Article');
    }
}
