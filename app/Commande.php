<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Commande extends Model
{
    protected $table = 'commandes';

    protected $fillable = [
        'code_client', 'article_id', 'conditionnement', 'stock_actuel_boraplast', 'nombre_palettes', 'date_commande', 'confirmation'
    ];


    public function article()
    {
        return $this->belongsTo('\App\Article');
    }


}
