<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;

use Carbon\Carbon;

use Date;

class NombreMois extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        //

        $date_current = Date::now();

        $date_current_hebdo = Date::now();

        return view('widgets.nombre_mois', [
            'config' => $this->config,
            'date_current' => $date_current,
            'date_current_hebdo' => $date_current_hebdo
        ]);
    }
}
