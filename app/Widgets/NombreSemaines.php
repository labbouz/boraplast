<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;

use Carbon\Carbon;

class NombreSemaines extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        //

        $now = Carbon::now();
        $weekOfYear =  $now->weekOfYear;
        $Year =  $now->year;

        return view('widgets.nombre_semaines', [
            'config' => $this->config,
            'weekOfYear' => $weekOfYear,
            'Year' => $Year,
        ]);
    }
}
