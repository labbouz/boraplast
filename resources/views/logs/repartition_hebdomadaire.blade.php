<div class="row">
    <div class="col-sm-12">
        <div class="white-box">
            <?php
            /*
            <pre>
                <?php print_r($operation) ?>
            </pre>
            */
            ?>
            <h3 class="box-title">Répartition Hebdomadaire selon les défalcations journalières</h3>
            <p class="text-muted">l'opération a été faite en <code>{{ $trace->created_at->format('d/m/Y H:i:s') }}</code></p>
            <div class="table-responsive">
                <table class="table color-table prevesions-hebdomadaires-table">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Status</th>
                        <th>Article</th>
                        <th>Date</th>
                        <th>Nombre de palettes</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if($operation['details'])
                    @foreach($operation['details'] as $key => $repartition)
                    <tr>
                        <td>{{ $key+1 }}</td>
                        <td>
                            @if( $repartition['status'] == 'insertion')
                                <span class="label label-rouded label-info">nouveau</span>
                            @else
                                <span class="label label-rouded label-danger">mise à jour</span>
                            @endif
                        </td>

                        <td>{{ $articles[$repartition['article_id']]->designation }} <span class="label-purple label">{{ $articles[$repartition['article_id']]->reference }}</span></td>
                        <td>
                            <?php
                            $date = Carbon\Carbon::parse($repartition['date_commande'].' 01:01:01');
                            ?>
                            {{ '@'.$jour[$date->format('N')] }} - <strong>{{ $date->format('d/m/Y') }}</strong>
                        </td>
                        <td>

                            @if( $repartition['status'] == 'changement')

                                @if( $repartition['old_nombre_palettes'] > 0 )
                                    <span class="label label-rouded label-danger">{{ $repartition['old_nombre_palettes'] }}</span>
                                @else
                                    <span class="label label-warning label-rouded">{{ $repartition['old_nombre_palettes'] }}</span>
                                @endif

                                 --->

                            @endif

                            @if( $repartition['new_nombre_palettes'] > 0 )
                                <span class="label label-success label-rouded">{{ $repartition['new_nombre_palettes'] }}</span>
                            @else
                                <span class="label label-warning label-rouded">{{ $repartition['new_nombre_palettes'] }}</span>
                            @endif
                            </td>
                    </tr>
                    @endforeach

                    @else
                        <tr>
                            <td colspan="8" class="text-center">Aucune modification n'a été faite</td>
                        </tr>
                    @endif

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>