@extends('layouts.app')

@section('title', 'Planning Besoin - '.ucfirst($date_month->format('F Y')))

@section('style')

    <!-- Editable CSS -->
    <link href="{{ asset('backend/plugins/bower_components/jquery-datatables-editable/datatables.css') }}" rel="stylesheet">

    <!--alerts CSS -->
    <link href="{{ asset('backend/plugins/bower_components/sweetalert/sweetalert.css') }}" rel="stylesheet">
    
@endsection

@section('content')
    <!-- .row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="white-box">
                <h3 class="box-title">{{ $date_month->format('F Y') }}</h3>
                <p class="text-muted">
                    Date mise à jour NAV : <strong>{{ $date_last_update->format('d/m/Y H:i') }}</strong>
                </p>

                <div class="table-responsive">
                <table class="table table-hover table-bordered color-bordered-table inverse-bordered-table table-previsions" id="editable-datatable">
                    <thead>
                    <?php
                    $array_col_indicateur = array();
                    $_index_day = 1;
                    ?>
                    <tr>
                        <th rowspan="2">Référence</th>
                        <th rowspan="2"><span data-toggle="tooltip" title="Conditionnement Article">Cond.</span></th>
                        <th rowspan="2"><span data-toggle="tooltip" title="Stock magasin BORAPLAST">Stock<br></span></th>


                        @foreach ($date['schedule'] as $date_semaine)
                            <th class="seperator"></th>
                            <th colspan="2" class="text-center">
                                <?php
                                $array_col_indicateur[$_index_day] = 'S '.$date_semaine['semaine'];
                                ?>

                                <big>S {{ $date_semaine['semaine'] }}</big>
                                <br>
                                <small>
                                    Du {{ $date_semaine['debut_semaine']->format('d/m/Y') }}<br>
                                    Au {{ $date_semaine['fin_semaine']->format('d/m/Y') }}</small>
                            </th>
                            <?php
                            $_index_day++;
                            ?>
                        @endforeach



                    </tr>


                    <tr>
                        @foreach ($date['schedule'] as $date_semaine)
                            <th class="seperator"></th>
                            <th data-toggle="tooltip" title="La quantité des articles" class="text-center">Qté</th>
                            <th data-toggle="tooltip" title="Nombre de palettes" class="text-center">NP</th>
                        @endforeach
                    </tr>



                    </thead>
                    <tbody>
                    <?php
                    $nb_pallete_total = 0;
                    $quantite_total = 0;
                    $week_current = intval(date('W'));
                    ?>
                    @foreach ($articles_actuels as $article_actuel)

                        <tr id="{{ $article_actuel->article_id }}" class="gradeX">
                            <th><span data-toggle="tooltip" title="{{ $article_actuel->designation }}">{{ $article_actuel->reference }}</span></th>
                            <th data-toggle="tooltip" title="Cond.">{{ number_format($article_actuel->conditionnement,0,',', ' ') }}<input type="hidden" id="conditionnement_{{ $article_actuel->article_id }}" value="{{ intval($article_actuel->conditionnement) }}"></th>
                            <th data-toggle="tooltip" title="Stock">{{ number_format($article_actuel->stock_actuel_boraplast,0,',', ' ') }}</th>

                            <?php
                            $index_col = 1;
                            ?>
                            @foreach ($date['schedule'] as $date_semaine)
                                <?php
                                if(isset($previsions[ $article_actuel->article_id ][ $date_semaine['semaine'] ])) {
                                    $nb_pallete = $previsions[ $article_actuel->article_id ][ $date_semaine['semaine'] ];
                                } else {
                                    $nb_pallete = 0;
                                }

                                $nb_pallete_total = $nb_pallete_total + $nb_pallete;
                                $quantite = $nb_pallete * $article_actuel->conditionnement;

                                $quantite_total = $quantite_total + $quantite;

                                if( $anne == date('Y') && intval($date_semaine['semaine']) < $week_current) {
                                    $activation_update_2 = 0;
                                } else {
                                    $activation_update_2 = $activation_update;
                                }

                                ?>
                                    <th class="seperator"></th>
                                    <th data-toggle="tooltip" title="Qté : {{ $array_col_indicateur[$index_col] }}" class="quantite" id="quantite_{{ $article_actuel->article_id }}_{{ $date_semaine['anne'] }}_{{ $date_semaine['semaine'] }}">{{ number_format($quantite,0,',', ' ') }}</th>

                                @if($activation_update_2)
                                    <td data-toggle="tooltip" title="NP : {{ $array_col_indicateur[$index_col] }}" data-artile="{{ $article_actuel->article_id }}" data-day-year="{{ $date_semaine['anne'] }}_{{ $date_semaine['semaine'] }}" id="nb_palettes_{{ $article_actuel->article_id }}_{{ $date_semaine['anne'] }}_{{ $date_semaine['semaine'] }}">{{ $nb_pallete }}</td>
                                @else
                                    <th data-toggle="tooltip" title="NP : {{ $array_col_indicateur[$index_col] }}" data-artile="{{ $article_actuel->article_id }}" data-day-year="{{ $date_semaine['anne'] }}_{{ $date_semaine['semaine'] }}" id="nb_palettes_{{ $article_actuel->article_id }}_{{ $date_semaine['anne'] }}_{{ $date_semaine['semaine'] }}" class="disabled">{{ $nb_pallete }}</th>
                                @endif

                                    <?php
                                    $index_col++;
                                    ?>

                            @endforeach





                        </tr>

                    @endforeach



                    </tbody>
                    <tfoot>
                    <tr>
                        <th class="active">Total</th>
                        <th class="active"></th>
                        <th class="active"></th>
                        @foreach ($date['schedule'] as $date_semaine)

                            <?php
                            if(isset($data_previsions_total[ $date_semaine['semaine'] ]['quantittes'])) {
                                $total_quantity =  $data_previsions_total[ $date_semaine['semaine'] ]['quantittes'];
                            } else {
                                $total_quantity =  0;
                            }

                            if(isset($data_previsions_total[ $date_semaine['semaine'] ]['nombre_palettes'])) {
                                $total_nombre_palettes = $data_previsions_total[ $date_semaine['semaine'] ]['nombre_palettes'];
                            } else {
                                $total_nombre_palettes =  0;
                            }
                            ?>
                                <th class="seperator"></th>
                            <th data-toggle="tooltip" title="Total Qté" class="quantite" id="total_quantitees_{{ $date_semaine['anne'] }}_{{ $date_semaine['semaine'] }}">{{ number_format($total_quantity,0,',', ' ') }}</th>
                            <th data-toggle="tooltip" title="Total NP" class="active" id="total_nb_palettes_{{ $date_semaine['anne'] }}_{{ $date_semaine['semaine'] }}">{{ $total_nombre_palettes }}</th>


                        @endforeach

                    </tr>
                    </tfoot>
                </table>
                </div>
            </div>
        </div>



        @if($activation_update)
        <div class="col-lg-12">
            <form id="form-commande" method="post" action="{{ route('previsions.create') }}">
                {{ csrf_field() }}

                <input type="hidden" name="nombre_mois" value="{{ $nombre_mois }}">
                <input type="hidden" name="anne_cible" value="{{ $anne }}">

                @foreach ($articles_actuels as $article_actuel)
                    @foreach ($date['schedule'] as $date_semaine)

                        <?php
                        if(isset($previsions[ $article_actuel->article_id ][ $date_semaine['semaine'] ])) {
                            $nb_pallete = $previsions[ $article_actuel->article_id ][ $date_semaine['semaine'] ];
                        } else {
                            $nb_pallete = 0;
                        }
                        ?>

                        <input type="hidden" id="{{ $article_actuel->article_id }}_{{ $date_semaine['anne'] }}_{{ $date_semaine['semaine'] }}" name="quantitee_article_{{ $article_actuel->article_id }}_{{ $date_semaine['anne'] }}_{{ $date_semaine['semaine'] }}" value="{{ $nb_pallete }}">
                    @endforeach
                @endforeach


            </form>
            <button id="send-form" class="btn btn-info btn-md btn-block send-commande" data-ajax="form-commande">Confirmer</button>
        </div>
        @endif
    </div>
    <!-- /.row -->



@endsection

@section('scripts')

    <!-- Sweet-Alert  -->
    <script src="{{ asset('backend/plugins/bower_components/sweetalert/sweetalert.min.js') }}"></script>
    <script src="{{ asset('backend/plugins/bower_components/sweetalert/jquery.sweet-alert.custom.js') }}"></script>

    <script src="{{ asset('backend/eliteadmin/js/custom.min.js') }}"></script>

    <!-- Editable -->
    <script src="{{ asset('backend/plugins/bower_components/jquery-datatables-editable/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('backend/plugins/bower_components/datatables/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('backend/plugins/bower_components/tiny-editable/mindmup-editabletable.js') }}"></script>
    <script src="{{ asset('backend/plugins/bower_components/tiny-editable/numeric-input-example.js') }}"></script>
    <script>
        $('#editable-datatable').editableTableWidget().numericInputExample().find('td:first').focus();
        $(document).ready(function() {

            $('td[readonly]').bind('click dblclick',function(e) {
                e.preventDefault();
                e.stopPropagation();
            });

            $('#editable-datatable td').on('change', function(evt, newValue) {
                // do something with the new cell value

                if(newValue<0) {
                    evt.stopPropagation();
                    evt.preventDefault();
                    return false;
                }

                var np = $(this).text();
                var _idArtile = $(this).attr("data-artile");
                var _dayYear = $(this).attr("data-day-year");

                var _conditionnement = $('#conditionnement_'+_idArtile).val();

                var _quantite = parseInt(_conditionnement) * parseInt(np);


                $("#quantite_"+_idArtile+"_"+_dayYear).text(number_format(_quantite, 0, ',' , ' '));

                $("#"+_idArtile+"_"+_dayYear).val(np);

                setTotalQuantitee(_dayYear)


            });

            function setTotalQuantitee(_dayYear) {

                var _total_quantitee = 0;

                $('#editable-datatable > tbody  > tr').each( function() {
                        var _idArtile = $(this).attr("id");

                    _total_quantitee += parseInt($("#quantite_"+_idArtile+"_"+_dayYear).text().replace(' ',''));

                });

                $("#total_quantitees_"+_dayYear).text(number_format(_total_quantitee, 0, ',' , ' '));
            }



            //Warning Message
            $('.send-commande').click(function(){

                var varBtn = $(this);
                var varForm = $(this).attr('data-ajax');
                var form = $('#'+varForm);

                var day_number = $(this).attr('data-day');

                swal({
                    title: "Etes vous sûre de vouloir valider la prévision ?",
                    text: "Vous pouvez modifier votre prévision à tout moment.",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes",
                    closeOnConfirm: false,
                    showLoaderOnConfirm: true

                }, function(){
                    $.ajax({
                        type: 'post',
                        url: form.attr( 'action' ),
                        data: form.serialize(),
                        dataType: 'json',
                        success: function(data) {
                            // success logic
                            console.log(data);

                            if(data.status == 'success') {
                                swal("Enregistré!", "Vos quantités prévisionnelles ont bien été enregistrer. Vous recevrez bientôt un mail de suivi.", "success");
                                //viderCol(day_number);
                                //varBtn.remove();
                            } else {
                                console.log(data);
                                swal("Erreur!", "PB DATABASE.", "error");
                            }
                        },
                        error: function(data){
                            console.log(data);
                            swal("Erreur!", "PB REQUEST.", "error");
                        }
                    });
                });
            });

            /*
            function viderCol(_dayYear) {

                $('#editable-datatable > tbody  > tr').each( function() {
                    var _idArtile = $(this).attr("id");

                    $("#quantite_"+_idArtile+"_"+_dayYear).text('').attr('class', 'quantite disabled');
                    $("#nb_palettes_"+_idArtile+"_"+_dayYear).text('').attr('class', 'disabled').attr('readonly', true);

                });

                $("#total_quantitees_"+_dayYear).text('').attr('class', 'quantite disabled');
                $("#total_nb_palettes_"+_dayYear).text('').attr('class', 'disabled');
            }
            */

            function number_format (number, decimals, dec_point, thousands_sep) {
                // Strip all characters but numerical ones.
                number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
                var n = !isFinite(+number) ? 0 : +number,
                    prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
                    sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
                    dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
                    s = '',
                    toFixedFix = function (n, prec) {
                        var k = Math.pow(10, prec);
                        return '' + Math.round(n * k) / k;
                    };
                // Fix for IE parseFloat(0.55).toFixed(0) = 0;
                s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
                if (s[0].length > 3) {
                    s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
                }
                if ((s[1] || '').length < prec) {
                    s[1] = s[1] || '';
                    s[1] += new Array(prec - s[1].length + 1).join('0');
                }
                return s.join(dec);
            }


        });
    </script>

@endsection


