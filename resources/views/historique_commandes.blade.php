@extends('layouts.app')

@section('title', 'Historique Commandes semaine '.$nombre_semaine)

@section('style')

    <!-- Editable CSS -->
    <link href="{{ asset('backend/plugins/bower_components/jquery-datatables-editable/datatables.css') }}" rel="stylesheet">
    
@endsection

@section('content')
    <!-- .row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="white-box">
                <h3 class="box-title">Semaine {{ $nombre_semaine }}</h3>
                <p class="text-muted">

                    du <strong>{{ $date->format('l j F Y') }}</strong> au <strong>{{ $date->addDay(6)->format('l j F Y') }}</strong>


                </p>


                @if(count($data_articles)>0)
                <div class="table-responsive">
                <table class="table table-hover table-bordered color-bordered-table inverse-bordered-table table-historique-commades-v1" id="editable-datatable">
                    <thead>
                    <?php
                    $array_col_indicateur = array();
                    $_index_day = 1;
                    ?>
                    <tr>
                        <th rowspan="2">Référence</th>

                        @foreach ($date_semaine as $date_jour)
                            <th class="seperator"></th>
                            <?php
                            $array_col_indicateur[$_index_day] = strtoupper(substr($date_jour['date_active']->format('l'), 0, 3)) . ' ' . $date_jour['date_active']->format('j') . '/' . $date_jour['date_active']->format('n');
                            ?>
                            <th colspan="4" class="text-center"><small>{{ $array_col_indicateur[$_index_day] }}</small></th>
                            <?php
                            $_index_day++;
                            ?>
                        @endforeach
                    </tr>

                    <tr>
                        @foreach ($date_semaine as $date_jour)
                            <th class="seperator"></th>
                            <th>Cond.</th>
                            <th>Stock</th>
                            <th>Qté</th>
                            <th>NP</th>
                        @endforeach
                    </tr>


                    </thead>
                    <tbody>

                    @foreach ($data_articles as $id_article => $data_article)

                        <tr id="{{ $id_article }}" class="gradeX">
                            <th class="separation"><span data-toggle="tooltip" title="{{ $data_article['designation'] }}">{{ $data_article['reference'] }}</span></th>


                            <?php
                            $index_col = 1;
                            ?>
                            @foreach ($date_semaine as $date_jour)
                                <th class="seperator"></th>

                                @if($date_jour['active'])


                                    @if (array_key_exists($date_jour['date_active']->format('z'), $data_commandes[$id_article]))

                                        <td data-toggle="tooltip" title="Cond. : {{ $array_col_indicateur[$index_col] }}">{{ number_format($data_commandes[$id_article][$date_jour['date_active']->format('z')]['conditionnement'],0, ',', ' ' ) }}</td>
                                        <td data-toggle="tooltip" title="Stock : {{ $array_col_indicateur[$index_col] }}">{{ number_format($data_commandes[$id_article][$date_jour['date_active']->format('z')]['stock_actuel_boraplast'],0, ',', ' ' ) }}</td>
                                        <td data-toggle="tooltip" title="Qté : {{ $array_col_indicateur[$index_col] }}" class="quantite">{{ $data_commandes[$id_article][$date_jour['date_active']->format('z')]['quantittes'] }}</td>
                                        <td data-toggle="tooltip" title="NP : {{ $array_col_indicateur[$index_col] }}" class="separation">{{ $data_commandes[$id_article][$date_jour['date_active']->format('z')]['nombre_palettes'] }}</td>

                                    @else

                                        <td data-toggle="tooltip" title="Cond. : {{ $array_col_indicateur[$index_col] }}">0</td>
                                        <td data-toggle="tooltip" title="Stock : {{ $array_col_indicateur[$index_col] }}">0</td>
                                        <td data-toggle="tooltip" title="Qté : {{ $array_col_indicateur[$index_col] }}" class="quantite">0</td>
                                        <td data-toggle="tooltip" title="NP : {{ $array_col_indicateur[$index_col] }}" class="separation">0</td>

                                    @endif




                                @else
                                    <th class="quantite disabled condition"></th>
                                    <th class="disabled" readonly></th>
                                    <th class="disabled" readonly></th>
                                    <th class="disabled separation" readonly></th>


                                @endif
                                <?php
                                $index_col++;
                                ?>
                            @endforeach



                        </tr>

                    @endforeach



                    </tbody>
                    <tfoot>
                    <tr>
                        <th class="active">Total</th>

                        @foreach ($date_semaine as $date_jour)
                            <th class="seperator"></th>

                            @if($date_jour['active'])
                                <th class="active"></th>
                                <th class="active"></th>
                                <th class="quantite" id="total_quantitees">{{ number_format ($data_commandes_total[$date_jour['date_active']->format('z')]['quantittes'], 0, ',', ' ' ) }}</th>
                                <th class="active">{{ $data_commandes_total[$date_jour['date_active']->format('z')]['nombre_palettes'] }}</th>

                            @else
                                <th class="disabled"></th>
                                <th class="disabled"></th>
                                <th class="quantite disabled"></th>
                                <th class="disabled"></th>

                            @endif


                        @endforeach
                    </tr>
                    </tfoot>
                </table>
                </div>
                @else

                <p>Il n'existe aucune demande sur cette semaine</p>

                @endif
            </div>
        </div>
    </div>
    <!-- /.row -->

@endsection

@section('scripts')
    <script src="{{ asset('backend/eliteadmin/js/custom.min.js') }}"></script>

    <script>

        $(document).ready(function() {


        });
    </script>

@endsection


