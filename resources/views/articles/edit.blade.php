@extends('layouts.app')

@section('title', "Modification Article")

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <div class="white-box">
                <h3 class="box-title m-b-0"><i class='fa fa-shopping-cart'></i> Modifier l'article {{ $article->reference }} </h3>

                {{ Form::model($article, array('route' => array('articles.update', $article->id), 'method' => 'PUT', 'class' => 'form-material form-horizontal m-t-30')) }}

                <div class="form-group">
                    {{ Form::label('reference', 'Référence') }}<br>
                    {{ Form::text('reference', null, array('class' => 'form-control')) }}
                </div>


                <div class="form-group">
                    {{ Form::label('designation', 'Designation') }}<br>
                    {{ Form::text('designation', null, array('class' => 'form-control')) }}
                </div>

                <div class="form-group">
                    {{ Form::label('unite', 'Unité') }}<br>
                    {{ Form::text('unite', null, array('class' => 'form-control')) }}
                </div>

                {{ Form::submit('Modifer un article', array('class' => 'btn btn-primary')) }}

                {{ Form::close() }}

            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script src="{{ asset('backend/eliteadmin/js/custom.min.js') }}"></script>

    <script>
        $(document).ready(function() {


        });
    </script>

@endsection