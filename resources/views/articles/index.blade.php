@extends('layouts.app')

@section('title', 'Tous les articles')

@section('content')
    <!-- .row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="white-box">
                <h3 class="box-title"><i class="fa fa-shopping-cart"></i> Gestion des articles</h3>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">

                        <thead>
                        <tr>
                            <th>Référence</th>
                            <th>Designation</th>
                            <th>Unité</th>
                            <th>Date de création</th>
                            <th>Operations</th>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach ($articles as $article)
                            <tr>

                                <td>{{ $article->reference }}</td>
                                <td>{{ $article->designation }}</td>
                                <td>{{ $article->unite }}</td>
                                <td>{{ $article->created_at->format('d/m/Y h:i') }}</td>

                                <td>
                                    <a href="{{ route('articles.edit', $article->id) }}" class="btn btn-info pull-left" style="margin-right: 3px;">Modifier</a>

                                    {!! Form::open(['method' => 'DELETE', 'route' => ['articles.destroy', $article->id] ]) !!}
                                    {!! Form::submit('Supprimer', ['class' => 'btn btn-danger']) !!}
                                    {!! Form::close() !!}

                                </td>
                            </tr>
                        @endforeach
                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('backend/eliteadmin/js/custom.min.js') }}"></script>

    <script>
        $(document).ready(function() {


        });
    </script>

@endsection