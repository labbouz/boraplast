@extends('layouts.app')

@section('title', 'Ajouter un article')

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <div class="white-box">
                <h3 class="box-title"><i class='fa fa-shopping-cart'></i> Nouveau article</h3>

                {{ Form::open(array('route' => 'articles.store', 'class' => 'form-material form-horizontal m-t-30')) }}

                <div class="form-group">
                    {{ Form::label('reference', 'Référence') }}<br>
                    {{ Form::text('reference', null, array('class' => 'form-control')) }}
                </div>


                <div class="form-group">
                    {{ Form::label('designation', 'Designation') }}<br>
                    {{ Form::text('designation', null, array('class' => 'form-control')) }}
                </div>

                <div class="form-group">
                    {{ Form::label('unite', 'Unité') }}<br>
                    {{ Form::text('unite', null, array('class' => 'form-control')) }}
                </div>

                {{ Form::submit('Créer Article', array('class' => 'btn btn-success btn-lg btn-block')) }}

                {{ Form::close() }}

            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script src="{{ asset('backend/eliteadmin/js/custom.min.js') }}"></script>

    <script>
        $(document).ready(function() {


        });
    </script>

@endsection