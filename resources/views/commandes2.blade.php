@extends('layouts.app')

@section('title', 'Commande journalière')

@section('style')

    <!-- Editable CSS -->
    <link href="{{ asset('backend/plugins/bower_components/jquery-datatables-editable/datatables.css') }}" rel="stylesheet">

    <!--alerts CSS -->
    <link href="{{ asset('backend/plugins/bower_components/sweetalert/sweetalert.css') }}" rel="stylesheet">
    
@endsection

@section('content')
    <!-- .row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="white-box">
                <h3 class="box-title">Semaine {{ $nombre_semaine }}</h3>
                {{--
                <h4><strong>Prévisions en total</strong>
                    <br>Nombre de palettes = {{ $data_previsions_total['nombre_palettes'] }}
                    <br>La quantité des articles = {{ $data_previsions_total['quantittes'] }}</h4>
                    --}}
                <p class="text-muted">

                    Du <strong>{{ ucfirst($date->format('l j F Y')) }}</strong> Au <strong>{{ ucfirst($date->addDay(6)->format('l j F Y')) }}</strong>
                    <br>

                    Jour de commande diponible

                    @if(count($jours_disponible) > 0)

                        @foreach ($jours_disponible as $jour_disponible)
                             / <strong>{{ ucfirst($jour_disponible) }}</strong>
                        @endforeach

                    @else
                        / <strong>jour +{{ Auth::user()->jour_commande }}</strong>
                    @endif



                <br>
                    Date mise à jour NAV : <strong>{{ $date_last_update->format('d/m/Y H:i') }}</strong>
                </p>


                <div class="table-responsive">
                <table class="table table-hover table-bordered color-bordered-table inverse-bordered-table table-commades-v2" id="editable-datatable">
                    <thead>
                    <?php
                    $array_col_indicateur = array();
                    $_index_day = 1;
                    ?>
                    <tr>
                        <th rowspan="2">Référence</th>
                        <th rowspan="2"><span data-toggle="tooltip" title="Conditionnement Article">Cond.</span></th>
                        <th rowspan="2"><span data-toggle="tooltip" title="Stock magasin BORAPLAST">Stock<br></span></th>
                        @foreach ($date_semaine as $date_jour)
                            <th class="seperator"></th>
                            <?php
                            $array_col_indicateur[$_index_day] = strtoupper(substr($date_jour['date_active']->format('l'), 0, 3)) . ' ' . $date_jour['date_active']->format('j') . '/' . $date_jour['date_active']->format('n');
                            ?>
                            <th colspan="2" class="text-center"><small>{{ strtoupper(substr($date_jour['date_active']->format('l'), 0, 3)) }} {{ $date_jour['date_active']->format('j') }}/{{ $date_jour['date_active']->format('n') }}</small></th>
                            <?php
                            $_index_day++;
                            ?>
                        @endforeach
                        <th class="seperator"></th>
                        <th colspan="2">Total</th>
                    </tr>

                    <tr>
                        @foreach ($date_semaine as $date_jour)
                            <th class="seperator"></th>
                            <th data-toggle="tooltip" title="La quantité des articles" class="text-center">Qté</th>
                            <th data-toggle="tooltip" title="Nombre de palettes" class="text-center">NP</th>
                        @endforeach
                            <th class="seperator"></th>
                            <th data-toggle="tooltip" title="La quantité des articles total" class="text-center">Qté</th>
                            <th data-toggle="tooltip" title="Nombre de palettes total" class="text-center">NP</th>
                    </tr>


                    </thead>
                    <tbody>
                    <?php
                    $nombre_totale_by_all_article=0;
                    $qt_totale_by_all_article=0;
                    ?>
                    @foreach ($articles_actuels as $article_actuel)

                        <tr id="{{ $article_actuel->article_id }}" class="gradeX">
                            <th><span data-toggle="tooltip" title="{{ $article_actuel->designation }}">{{ $article_actuel->reference }}</span></th>
                            <th data-toggle="tooltip" title="Cond.">{{ number_format($article_actuel->conditionnement, 0, ',', ' ') }}<input type="hidden" id="conditionnement_{{ $article_actuel->article_id }}" value="{{ intval($article_actuel->conditionnement) }}"></th>
                            <th data-toggle="tooltip" title="Stock">{{ number_format($article_actuel->stock_actuel_boraplast, 0, ',', ' ') }}</th>

                            <?php
                            $nombre_totale_by_article=0;
                            $qt_totale_by_article=0;

                            $index_col = 1;
                            ?>
                            @foreach ($date_semaine as $date_jour)

                                <?php
                                $nombre_totale_by_article+=$data_previsions_c[$article_actuel->article_id][$date_jour['date_active']->format('Y-m-d')];
                                $qt_totale_by_article+=$data_previsions_c[$article_actuel->article_id][$date_jour['date_active']->format('Y-m-d')] * $article_actuel->conditionnement;
                                ?>
                                    <th class="seperator"></th>
                                @if($date_jour['active'])
                                    <?php $quantity_a_afficher = $article_actuel->conditionnement * $data_previsions_c[$article_actuel->article_id][$date_jour['date_active']->format('Y-m-d')]; ?>
                                    <th data-toggle="tooltip" title="Qté : {{ $array_col_indicateur[$index_col] }}" class="quantite" id="quantite_{{ $article_actuel->article_id }}_{{ $date_jour['date_active']->format('z') }}">{{ number_format($quantity_a_afficher, 0, ',', ' ') }}</th>
                                    <td data-toggle="tooltip" title="NP : {{ $array_col_indicateur[$index_col] }}" data-artile="{{ $article_actuel->article_id }}" data-day-year="{{ $date_jour['date_active']->format('z') }}" id="nb_palettes_{{ $article_actuel->article_id }}_{{ $date_jour['date_active']->format('z') }}">{{ $data_previsions_c[$article_actuel->article_id][$date_jour['date_active']->format('Y-m-d')] }}</td>

                                @else
                                    <th data-toggle="tooltip" title="Qté : {{ $array_col_indicateur[$index_col] }}" class="quantite disabled">{{ number_format($article_actuel->conditionnement * $data_previsions_c[$article_actuel->article_id][$date_jour['date_active']->format('Y-m-d')],0,',',' ') }}</th>
                                    <td data-toggle="tooltip" title="NP : {{ $array_col_indicateur[$index_col] }}" class="disabled" readonly>{{ $data_previsions_c[$article_actuel->article_id][$date_jour['date_active']->format('Y-m-d')] }}</td>

                                @endif
                                    <?php
                                    $index_col++;
                                    ?>

                            @endforeach
                            <th class="seperator"></th>
                            <th data-toggle="tooltip" title="Total Qté par article" class="text-center">{{ number_format($qt_totale_by_article, 0, ',', ' ') }}</th>
                            <th data-toggle="tooltip" title="Total NP par article" class="text-center">{{ $nombre_totale_by_article }}</th>

                            <?php
                            $nombre_totale_by_all_article+=$nombre_totale_by_article;
                            $qt_totale_by_all_article+=$qt_totale_by_article;
                            ?>

                        </tr>

                    @endforeach



                    </tbody>
                    <tfoot>
                    <tr>
                        <th class="active">Total</th>
                        <th class="active"></th>
                        <th class="active"></th>
                        @foreach ($date_semaine as $date_jour)
                            <th class="seperator"></th>
                            @if($date_jour['active'])
                                <th data-toggle="tooltip" title="Total Qté" class="quantite" id="total_quantitees_{{ $date_jour['date_active']->format('z') }}">{{ number_format($data_total_commande[$date_jour['date_active']->format('Y-m-d')]['qt'], 0, ',', ' ') }}</th>
                                <th data-toggle="tooltip" title="Total NP" class="active" id="total_nb_palettes_{{ $date_jour['date_active']->format('z') }}">{{ $data_total_commande[$date_jour['date_active']->format('Y-m-d')]['np'] }}</th>

                            @else
                                <th data-toggle="tooltip" title="Total Qté" class="quantite disabled">{{ number_format($data_total_commande[$date_jour['date_active']->format('Y-m-d')]['qt'], 0, ',', ' ') }}</th>
                                <th data-toggle="tooltip" title="Total NP" class="disabled">{{ $data_total_commande[$date_jour['date_active']->format('Y-m-d')]['np'] }}</th>

                            @endif

                        @endforeach
                        <th class="seperator"></th>
                        <th>{{ number_format($qt_totale_by_all_article,0,',', ' ') }}</th>
                        <th>{{ $nombre_totale_by_all_article }}</th>
                    </tr>
                    <tr>
                        <th colspan="3"></th>
                        @foreach ($date_semaine as $date_jour)
                            <th class="seperator"></th>
                            @if($date_jour['active'])
                                <th colspan="2" class="active buttons">
                                    <form id="form-commande-{{ $date_jour['date_active']->format('z') }}" method="post" action="{{ route('commande.create2') }}">
                                        {{ csrf_field() }}

                                        @foreach ($articles_actuels as $article_actuel)

                                            <input type="hidden" id="{{ $article_actuel->article_id }}_{{ $date_jour['date_active']->format('z') }}" name="quantitee_article_{{ $article_actuel->id }}" value="{{ $data_previsions_c[$article_actuel->article_id][$date_jour['date_active']->format('Y-m-d')] }}">

                                        @endforeach
                                            <input type="hidden" name="date_commande" value="{{ $date_jour['date_active']->format('Y-m-d') }}">

                                    </form>

                                    @if($le_jour_commande_active == $date_jour['date_active']->format('Y-m-d') && date('Y-m-d') >= $dernier_date_commande )
                                        <button id="send-form-{{ $date_jour['date_active']->format('z') }}" class="btn btn-info btn-md btn-block send-commande" data-ajax="form-commande-{{ $date_jour['date_active']->format('z') }}" data-day="{{ $date_jour['date_active']->format('z') }}">Commander</button>
                                    @else
                                        <button id="send-form-{{ $date_jour['date_active']->format('z') }}" class="btn btn-info btn-md btn-block disabled">Commander</button>
                                    @endif

                                </th>
                            @else
                                <th colspan="2" class="active"> </th>
                            @endif
                        @endforeach
                        <th class="seperator"></th>
                        <th colspan="2"></th>
                    </tr>
                    </tfoot>
                </table>
                    <a href="{{ route('previsions_hebdo', ['anne' => $date->format('Y'), 'nombre_mois' => $date->format('n')] ) }}" class="btn btn-info btn-md btn-block" > Répartition Hebdo pour mois {{ $date->format('F Y') }}</a>
                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->

@endsection

@section('scripts')

    <!-- Sweet-Alert  -->
    <script src="{{ asset('backend/plugins/bower_components/sweetalert/sweetalert.min.js') }}"></script>
    <script src="{{ asset('backend/plugins/bower_components/sweetalert/jquery.sweet-alert.custom.js') }}"></script>

    <script src="{{ asset('backend/eliteadmin/js/custom.min.js') }}"></script>

    <!-- Editable -->
    <script src="{{ asset('backend/plugins/bower_components/jquery-datatables-editable/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('backend/plugins/bower_components/datatables/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('backend/plugins/bower_components/tiny-editable/mindmup-editabletable.js') }}"></script>
    <script src="{{ asset('backend/plugins/bower_components/tiny-editable/numeric-input-example.js') }}"></script>
    <script>
        //$('#editable-datatable').editableTableWidget().numericInputExample().find('td:first').focus();
        $(document).ready(function() {

            /*
            $('td[readonly]').bind('click dblclick',function(e) {
                e.preventDefault();
                e.stopPropagation();
            });



            $('#editable-datatable td').on('change', function(evt, newValue) {
                // do something with the new cell value

                if(newValue<0) {
                    evt.stopPropagation();
                    evt.preventDefault();
                    return false;
                }

                var np = $(this).text();
                var _idArtile = $(this).attr("data-artile");
                var _dayYear = $(this).attr("data-day-year");

                var _conditionnement = $('#conditionnement_'+_idArtile).val();

                var _quantite = parseInt(_conditionnement) * parseInt(np);


                $("#quantite_"+_idArtile+"_"+_dayYear).text(number_format(_quantite, 0, ',' , ' '));

                $("#"+_idArtile+"_"+_dayYear).val(np);

                setTotalQuantitee(_dayYear)


            });


            function setTotalQuantitee(_dayYear) {

                var _total_quantitee = 0;

                $('#editable-datatable > tbody  > tr').each( function() {
                        var _idArtile = $(this).attr("id");

                    _total_quantitee += parseInt($("#quantite_"+_idArtile+"_"+_dayYear).text().replace(' ',''));

                });

                $("#total_quantitees_"+_dayYear).text(number_format(_total_quantitee, 0, ',' , ' '));
            }
             */



            //Warning Message
            $('.send-commande').click(function(){

                var varBtn = $(this);
                var varForm = $(this).attr('data-ajax');
                var form = $('#'+varForm);

                var day_number = $(this).attr('data-day');

                swal({
                    title: "Etes vous sûre de vouloir valider la commande ?",
                    text: "NB: vous ne pouvez pas faire aucune modification après la validation de la commande.",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes",
                    closeOnConfirm: false,
                    showLoaderOnConfirm: true

                }, function(){
                    $.ajax({
                        type: 'post',
                        url: form.attr( 'action' ),
                        data: form.serialize(),
                        dataType: 'json',
                        success: function(data) {
                            // success logic
                            console.log(data);

                            if(data.status == 'success') {
                                swal("Enregistré!", "Votre commande a été enregistré avec succès. Vous receverez bientôt un mail de confirmation.", "success");
                                //viderCol(day_number);
                                varBtn.remove();
                            } else {
                                console.log(data);
                                swal("Erreur!", "PB DATABASE.", "error");
                            }
                        },
                        error: function(data){
                            console.log(data);
                            swal("Erreur!", "PB REQUEST.", "error");
                        }
                    });
                });
            });

            /*
            function viderCol(_dayYear) {

                $('#editable-datatable > tbody  > tr').each( function() {
                    var _idArtile = $(this).attr("id");

                    $("#quantite_"+_idArtile+"_"+_dayYear).text('').attr('class', 'quantite disabled');
                    $("#nb_palettes_"+_idArtile+"_"+_dayYear).text('').attr('class', 'disabled').attr('readonly', true);

                });

                $("#total_quantitees_"+_dayYear).text('').attr('class', 'quantite disabled');
                $("#total_nb_palettes_"+_dayYear).text('').attr('class', 'disabled');
            }



            function number_format (number, decimals, dec_point, thousands_sep) {
                // Strip all characters but numerical ones.
                number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
                var n = !isFinite(+number) ? 0 : +number,
                    prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
                    sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
                    dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
                    s = '',
                    toFixedFix = function (n, prec) {
                        var k = Math.pow(10, prec);
                        return '' + Math.round(n * k) / k;
                    };
                // Fix for IE parseFloat(0.55).toFixed(0) = 0;
                s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
                if (s[0].length > 3) {
                    s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
                }
                if ((s[1] || '').length < prec) {
                    s[1] = s[1] || '';
                    s[1] += new Array(prec - s[1].length + 1).join('0');
                }
                return s.join(dec);
            }

*/
        });
    </script>

@endsection


