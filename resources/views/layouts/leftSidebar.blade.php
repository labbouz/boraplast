    <!-- Left navbar-header -->
        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse slimscrollsidebar">

                <ul class="nav" id="side-menu">
                    <li> <a href="{{ route('dashboard.display') }}" class="waves-effect"><i data-icon="P" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">Tableaux de bord</span></a> </li>

                    @hasrole('Admin')

                    <li> <a href="#" class="waves-effect"><i class="icon-people fa-fw"></i> <span class="hide-menu"> Les utilisateurs <span class="fa arrow"></span></span></a>

                        <ul class="nav nav-second-level">
                            <li> <a href="{{ route('users.index') }}">Tous les utilisateurs</a> </li>
                            <li> <a href="{{ route('users.create') }}">Ajouter un utilisateur</a> </li>
                        </ul>
                    </li>

                    <li> <a href="{{ route('roles.index') }}" class="waves-effect"><i class="icon-key fa-fw"></i> <span class="hide-menu">Roles</span></a> </li>
                    <li> <a href="{{ route('permissions.index') }}" class="waves-effect"><i class="icon-key fa-fw"></i> <span class="hide-menu">Autorisations</span></a> </li>

                    <li> <a href="#" class="waves-effect"><i class="icon-people fa-fw"></i> <span class="hide-menu"> Les articles <span class="fa arrow"></span></span></a>

                        <ul class="nav nav-second-level">
                            <li> <a href="{{ route('articles.index') }}">Tous les articles</a> </li>
                            <li> <a href="{{ route('articles.create') }}">Ajouter un article</a> </li>
                        </ul>
                    </li>

                    @else

                        @widget('nombreMois')

                        @widget('nombreSemaines')



                    @endhasrole



                    <li><a href="{{ route('logout') }}" class="waves-effect" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"><i class="icon-logout fa-fw"></i> <span class="hide-menu">Déconnexion</span></a></li>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>

                </ul>
            </div>
        </div>
        <!-- Left navbar-header end -->