<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    @include('layouts.head')

    @yield('style')
</head>

<body>
    @include('layouts.preloader')

    <div id="wrapper" class="login-register">
        <!-- Page Content -->
        <div class="login-box">
            @yield('content')

        </div>
        <!-- /#login-box -->
    </div>
    <!-- /#wrapper -->
    @include('layouts.script')
    <!-- Custom Theme JavaScript -->
    @yield('scripts')
</body>
</html>
