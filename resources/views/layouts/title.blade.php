<div class="row bg-title">
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <h4 class="page-title">@yield('title')</h4>
    </div>
    <div class="col-lg-6 col-sm-6 col-md-12 col-xs-12">
        @include('layouts.breadcrumbs')
    </div>
    <!-- /.col-lg-12 -->
</div>