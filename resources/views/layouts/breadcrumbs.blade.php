
@if (request()->route()->getName() == 'users.edit')

    {!! Breadcrumbs::render('users.edit', $user) !!}

@else

    @if (request()->route()->getName() == 'roles.edit')

        {!! Breadcrumbs::render('roles.edit', $role) !!}

    @else

        @if (request()->route()->getName() == 'permissions.edit')

            {!! Breadcrumbs::render('permissions.edit', $permission) !!}

        @else

            @if (request()->route()->getName() == 'articles.edit')

                {!! Breadcrumbs::render('articles.edit', $article) !!}

            @else

                {!! Breadcrumbs::render() !!}

            @endif

        @endif

    @endif

@endif
