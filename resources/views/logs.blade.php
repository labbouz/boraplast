@extends('layouts.app')

@section('title', 'Fichier LOG ' . $user->nom_societe)

@section('style')

    <!-- Editable CSS -->
    {{--
    <link href="{{ asset('backend/plugins/bower_components/jquery-datatables-editable/datatables.css') }}" rel="stylesheet">
    --}}
@endsection

@section('content')
    <!-- .row -->

        @foreach( $traces as $trace)
            <?php
            $operation = $trace->operation;
            ?>

            @include('logs.'.$operation['type_operation'])



        @endforeach

    <!-- /.row -->

@endsection

@section('scripts')
    <script src="{{ asset('backend/eliteadmin/js/custom.min.js') }}"></script>

    <!-- Editable -->
    {{--
    <script src="{{ asset('backend/plugins/bower_components/jquery-datatables-editable/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('backend/plugins/bower_components/datatables/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('backend/plugins/bower_components/tiny-editable/mindmup-editabletable.js') }}"></script>
    <script src="{{ asset('backend/plugins/bower_components/tiny-editable/numeric-input-example.js') }}"></script>
    <script>
        $('#mainTable').editableTableWidget().numericInputExample().find('td:first').focus();
        $('#editable-datatable').editableTableWidget().numericInputExample().find('td:first').focus();
        $(document).ready(function() {
            $('#editable-datatable').DataTable({
                paging: false
            });



        });
    </script>
--}}
@endsection


