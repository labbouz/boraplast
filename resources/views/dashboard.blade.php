@extends('layouts.app')

@section('title', 'Dashboard Admin')

@section('style')



@endsection

@section('content')
    <!-- .row -->
    <div class="row">
        <div class="col-md-3">
            <div class="white-box">
                <h3 class="box-title">Les clients</h3>
                <ul class="list-inline two-part">
                    <li><i class="icon-people text-info"></i></li>
                    <li class="text-right"><span class="counter">{{ $users->count() }}</span></li>
                </ul>
            </div>
        </div>

        <div class="col-md-3">
            <div class="white-box">
                <h3 class="box-title">LES ARTICLES</h3>
                <ul class="list-inline two-part">
                    <li><i class="icon-folder text-purple"></i></li>
                    <li class="text-right"><span class="counter">{{ $articles_actuels->count() }}</span></li>
                </ul>
            </div>
        </div>

        <div class="col-md-3">
            <div class="white-box">
                <h3 class="box-title">NP pour semaine {{ $week }}</h3>
                <ul class="list-inline two-part">
                    <li><i class="icon-basket-loaded text-danger"></i></li>
                    <li class="text-right"><span class="counter">{{ $commandes_semaine->sum('nombre_palettes') }}</span></li>
                </ul>
            </div>
        </div>

        <div class="col-md-3">
            <div class="white-box">
                <h3 class="box-title">NP pour {{ $mois }}</h3>
                <ul class="list-inline two-part">
                    <li><i class="icon-basket-loaded text-success"></i></li>
                    <li class="text-right"><span class="counter">{{ $commandes_mois->sum('nombre_palettes') }}</span></li>
                </ul>
            </div>
        </div>

    </div>

    @foreach( $users as $user)

        <div class="row">
            <div class="col-md-12 col-lg-12 col-sm-12">
                <div class="white-box">
                    <div class="row row-in">
                        <div class="col-lg-3 col-sm-6 row-in-br">
                            <div class="col-in row">
                                <div class="col-md-6 col-sm-6 col-xs-6"> <i class="ti-user"></i>
                                    <h5 class="text-muted vb"><a href="{{ route('file.log', $user->id) }}">{{ $user->nom_societe }}</a></h5> </div>
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <h3 class="counter text-right m-t-15 text-danger">{{ $user->code_client }}</h3> </div>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%"> <span class="sr-only">40% Complete (success)</span> </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-6 row-in-br  b-r-none">
                            <div class="col-in row">
                                <div class="col-md-6 col-sm-6 col-xs-6"> <i class="ti-menu-alt"></i>
                                    <h5 class="text-muted vb">LES ARTICLES</h5> </div>
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <h3 class="counter text-right m-t-15 text-info">{{ $user->articles->count() }}</h3> </div>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%"> <span class="sr-only">40% Complete (success)</span> </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-6 row-in-br">
                            <div class="col-in row">
                                <div class="col-md-6 col-sm-6 col-xs-6"> <i class="ti-calendar"></i>
                                    <h5 class="text-muted vb">Totale NP en Prévisions pour S{{ $week }}</h5> </div>
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <h3 class="counter text-right m-t-15 text-success">{{ $user->previsions->sum('nombre_palettes') }}</h3> </div>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%"> <span class="sr-only">40% Complete (success)</span> </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-6  b-0">
                            <div class="col-in row">
                                <div class="col-md-6 col-sm-6 col-xs-6"> <i class="ti-shopping-cart-full"></i>
                                    <h5 class="text-muted vb">Totale NP en COMMANDES pour S{{ $week }}</h5> </div>
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <h3 class="counter text-right m-t-15 text-warning">{{ $user->commandes->sum('nombre_palettes') }}</h3> </div>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%"> <span class="sr-only">40% Complete (success)</span> </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
    <!-- /.row -->

@endsection

@section('scripts')
    <script src="{{ asset('backend/eliteadmin/js/custom.min.js') }}"></script>

    <script>
        $(document).ready(function() {


        });
    </script>

@endsection


