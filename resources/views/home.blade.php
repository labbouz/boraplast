@extends('layouts.app')

@section('title', 'Commande journalière')

@section('style')

    <!-- Editable CSS -->
    <link href="{{ asset('backend/plugins/bower_components/jquery-datatables-editable/datatables.css') }}" rel="stylesheet">
    
@endsection

@section('content')
    <!-- .row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="white-box">
                <h3 class="box-title">Semaine 27</h3>
                <p class="text-muted">du 01 juin au 07 juin</p>
                <table class="table table-striped table-bordered" id="editable-datatable">
                    <thead>
                    <tr>
                        <th>Ref</th>
                        <th>Condionnement</th>
                        <th>Stock client</th>
                        <th>stock boraplast</th>
                        <th>01 juin</th>
                        <th>02 juin</th>
                        <th>03 juin</th>
                        <th>04 juin</th>
                        <th>05 juin</th>
                        <th>06 juin</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr id="1" class="gradeX">
                        <th>700001</th>
                        <th>1600</th>
                        <th></th>
                        <th>5,124</th>
                        <td class="center">0</td>
                        <td class="center">0</td>
                        <td class="center">0</td>
                        <td class="center">0</td>
                        <td class="center">0</td>
                        <td class="center">0</td>
                    </tr>
                    <tr id="2" class="gradeC">
                        <th>700004</th>
                        <th>1200</th>
                        <th></th>
                        <th>1,857</th>
                        <td class="center">0</td>
                        <td class="center">0</td>
                        <td class="center">0</td>
                        <td class="center">0</td>
                        <td class="center">0</td>
                        <td class="center">0</td>
                    </tr>
                    <tr id="3" class="gradeA">
                        <th>700005</th>
                        <th>1200</th>
                        <th></th>
                        <th>0</th>
                        <td class="center">0</td>
                        <td class="center">0</td>
                        <td class="center">0</td>
                        <td class="center">0</td>
                        <td class="center">0</td>
                        <td class="center">0</td>
                    </tr>
                    <tr id="4" class="gradeA">
                        <th>700007</th>
                        <th>1170</th>
                        <th></th>
                        <th>1,089</th>
                        <td class="center">0</td>
                        <td class="center">0</td>
                        <td class="center">0</td>
                        <td class="center">0</td>
                        <td class="center">0</td>
                        <td class="center">0</td>
                    </tr>
                    <tr id="5" class="gradeA">
                        <th>700016</th>
                        <th>810</th>
                        <th></th>
                        <th>4,886</th>
                        <td class="center">0</td>
                        <td class="center">0</td>
                        <td class="center">0</td>
                        <td class="center">0</td>
                        <td class="center">0</td>
                        <td class="center">0</td>
                    </tr>
                    <tr id="6" class="gradeA">
                        <th>700017</th>
                        <th>1350</th>
                        <th></th>
                        <th>2,920</th>
                        <td class="center">0</td>
                        <td class="center">0</td>
                        <td class="center">0</td>
                        <td class="center">0</td>
                        <td class="center">0</td>
                        <td class="center">0</td>
                    </tr>
                    <tr id="7" class="gradeA">
                        <th>700023</th>
                        <th>1170</th>
                        <th></th>
                        <th>2,900</th>
                        <td class="center">0</td>
                        <td class="center">0</td>
                        <td class="center">0</td>
                        <td class="center">0</td>
                        <td class="center">0</td>
                        <td class="center">0</td>
                    </tr>
                    <tr id="8" class="gradeA">
                        <th>700026</th>
                        <th>480</th>
                        <th></th>
                        <th>219</th>
                        <td class="center">0</td>
                        <td class="center">0</td>
                        <td class="center">0</td>
                        <td class="center">0</td>
                        <td class="center">0</td>
                        <td class="center">0</td>
                    </tr>
                    <tr id="9" class="gradeA">
                        <th>700028</th>
                        <th>2000</th>
                        <th></th>
                        <th>2,000</th>
                        <td class="center">0</td>
                        <td class="center">0</td>
                        <td class="center">0</td>
                        <td class="center">0</td>
                        <td class="center">0</td>
                        <td class="center">0</td>
                    </tr>
                    <tr id="10" class="gradeA">
                        <th>700029</th>
                        <th>2400</th>
                        <th></th>
                        <th>1,580</th>
                        <td class="center">0</td>
                        <td class="center">0</td>
                        <td class="center">0</td>
                        <td class="center">0</td>
                        <td class="center">0</td>
                        <td class="center">0</td>
                    </tr>
                    <tr id="11" class="gradeA">
                        <th>700035</th>
                        <th>600</th>
                        <th></th>
                        <th>957</th>
                        <td class="center">0</td>
                        <td class="center">0</td>
                        <td class="center">0</td>
                        <td class="center">0</td>
                        <td class="center">0</td>
                        <td class="center">0</td>
                    </tr>
                    <tr id="12" class="gradeA">
                        <th>700037</th>
                        <th>480</th>
                        <th></th>
                        <th>955</th>
                        <td class="center">0</td>
                        <td class="center">0</td>
                        <td class="center">0</td>
                        <td class="center">0</td>
                        <td class="center">0</td>
                        <td class="center">0</td>
                    </tr>
                    <tr id="13" class="gradeA">
                        <th>700042</th>
                        <th>1350</th>
                        <th></th>
                        <th>0</th>
                        <td class="center">0</td>
                        <td class="center">0</td>
                        <td class="center">0</td>
                        <td class="center">0</td>
                        <td class="center">0</td>
                        <td class="center">0</td>
                    </tr>
                    <tr id="14" class="gradeA">
                        <th>700047</th>
                        <th>540</th>
                        <th></th>
                        <th>2,028</th>
                        <td class="center">0</td>
                        <td class="center">0</td>
                        <td class="center">0</td>
                        <td class="center">0</td>
                        <td class="center">0</td>
                        <td class="center">0</td>
                    </tr>
                    <tr id="15" class="gradeA">
                        <th>700048</th>
                        <th>780</th>
                        <th></th>
                        <th>2,023</th>
                        <td class="center">0</td>
                        <td class="center">0</td>
                        <td class="center">0</td>
                        <td class="center">0</td>
                        <td class="center">0</td>
                        <td class="center">0</td>
                    </tr>
                    <tr id="16" class="gradeA">
                        <th>700053</th>
                        <th>810</th>
                        <th></th>
                        <th>0</th>
                        <td class="center">0</td>
                        <td class="center">0</td>
                        <td class="center">0</td>
                        <td class="center">0</td>
                        <td class="center">0</td>
                        <td class="center">0</td>
                    </tr>
                    <tr id="17" class="gradeA">
                        <th>700054</th>
                        <th>630</th>
                        <th></th>
                        <th>630</th>
                        <td class="center">0</td>
                        <td class="center">0</td>
                        <td class="center">0</td>
                        <td class="center">0</td>
                        <td class="center">0</td>
                        <td class="center">0</td>
                    </tr>
                    <tr id="18" class="gradeA">
                        <th>700057</th>
                        <th>780</th>
                        <th></th>
                        <th>0</th>
                        <td class="center">0</td>
                        <td class="center">0</td>
                        <td class="center">0</td>
                        <td class="center">0</td>
                        <td class="center">0</td>
                        <td class="center">0</td>
                    </tr>

                    </tbody>
                    <tfoot>
                    <tr>
                        <th>Total</th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th>0h</th>
                        <th>0h</th>
                        <th>0h</th>
                        <th>0h</th>
                        <th>0h</th>
                        <th>0h</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
        <div class="col-lg-12">
            <button class="btn btn-info btn-lg btn-block">Commander</button>
        </div>
    </div>
    <!-- /.row -->

@endsection

@section('scripts')
    <script src="{{ asset('backend/eliteadmin/js/custom.min.js') }}"></script>

    <!-- Editable -->
    <script src="{{ asset('backend/plugins/bower_components/jquery-datatables-editable/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('backend/plugins/bower_components/datatables/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('backend/plugins/bower_components/tiny-editable/mindmup-editabletable.js') }}"></script>
    <script src="{{ asset('backend/plugins/bower_components/tiny-editable/numeric-input-example.js') }}"></script>
    <script>
        $('#mainTable').editableTableWidget().numericInputExample().find('td:first').focus();
        $('#editable-datatable').editableTableWidget().numericInputExample().find('td:first').focus();
        $(document).ready(function() {
            $('#editable-datatable').DataTable({
                paging: false
            });



        });
    </script>

@endsection


