@extends('layouts.appLogin')

@section('title', 'Reset Password')

@section('style')

@endsection

@section('content')
    <div class="white-box">
        <form class="form-horizontal" role="form" method="POST" action="{{ route('password.email') }}">
            {{ csrf_field() }}
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif

            <h3>Reset Password</h3>
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">

                <div class="col-md-12">
                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="E-Mail Address" required>

                    @if ($errors->has('email'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-12">
                    <button class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Send Password Reset Link</button>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('backend/eliteadmin/js/custom.min.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function() {

        });
    </script>
@endsection
