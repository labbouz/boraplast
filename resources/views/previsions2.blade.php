@extends('layouts.app')

@section('title', 'Répartition hebdo - '.ucfirst($date_month->format('F Y')))

@section('style')

    <!-- Editable CSS -->
    <link href="{{ asset('backend/plugins/bower_components/jquery-datatables-editable/datatables.css') }}" rel="stylesheet">

    <!--alerts CSS -->
    <link href="{{ asset('backend/plugins/bower_components/sweetalert/sweetalert.css') }}" rel="stylesheet">
    
@endsection

@section('content')
    <!-- .row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="white-box">
                <h3 class="box-title">{{ $date_month->format('F Y') }}</h3>
                <p class="text-muted">
                    Date mise à jour NAV : <strong>{{ $date_last_update->format('d/m/Y H:i') }}</strong>
                </p>

                <div class="table-responsive">
                <table class="table table-hover table-bordered color-bordered-table inverse-bordered-table table-previsions-hebdo" id="editable-datatable">
                    <thead>
                    <?php
                    $array_col_indicateur = array();
                    $_index_day = 1;
                    ?>
                    <tr>
                        <th rowspan="2">Référence</th>
                        <th rowspan="2"><span data-toggle="tooltip" title="Conditionnement Article">Cond.</span></th>
                        <th rowspan="2"><span data-toggle="tooltip" title="Stock magasin BORAPLAST">Stock<br></span></th>


                        @foreach ($date['schedule'] as $date_semaine)
                            <?php
                            $array_col_indicateur[$_index_day] = 'S '.$date_semaine['semaine'];
                            ?>
                            <th class="seperator"></th>
                            <th colspan="3" class="text-center">
                                <big>S {{ $date_semaine['semaine'] }}</big>
                                <br>
                                <small>
                                    Du {{ $date_semaine['debut_semaine']->format('d/m/Y') }}<br>
                                    Au {{ $date_semaine['fin_semaine']->format('d/m/Y') }}</small>
                            </th>
                            <?php
                            $_index_day++;
                            ?>
                        @endforeach
                        <th class="seperator"></th>
                        <th colspan="3">Total</th>



                    </tr>


                    <tr>

                        @foreach ($date['schedule'] as $date_semaine)
                            <th class="seperator"></th>
                            <th data-toggle="tooltip" title="La quantité des articles" class="text-center">Qté</th>
                            <th data-toggle="tooltip" title="Nombre de palettes" class="text-center">NP</th>
                            <th data-toggle="tooltip" title="Nombre de palettes selon les défalcations journalières" class="text-center">NPR</th>
                        @endforeach
                            <th class="seperator"></th>
                            <th data-toggle="tooltip" title="La quantité des articles total" class="text-center">Qté</th>
                            <th data-toggle="tooltip" title="Nombre de palettes total" class="text-center">NP</th>
                            <th data-toggle="tooltip" title="Nombre de palettes selon les défalcations journalières total" class="text-center">NPR</th>
                    </tr>



                    </thead>
                    <tbody>
                    <?php
                    $nb_pallete_total = 0;
                    $quantite_total = 0;
                    $week_current = intval(date('W'));

                    $nombre_totale_by_all_article=0;
                    $qt_totale_by_all_article=0;
                    $nombre_totale_distrubuer_by_all_article=0;
                    ?>
                    @foreach ($articles_actuels as $article_actuel)

                        <tr id="{{ $article_actuel->article_id }}" class="gradeX">
                            <th><span data-toggle="tooltip" title="{{ $article_actuel->designation }}">{{ $article_actuel->reference }}</span></th>
                            <th data-toggle="tooltip" title="Conditionnement Article">{{ number_format($article_actuel->conditionnement,0,',', ' ') }}<input type="hidden" id="conditionnement_{{ $article_actuel->article_id }}" value="{{ intval($article_actuel->conditionnement) }}"></th>
                            <th data-toggle="tooltip" title="Stock magasin BORAPLAST">{{ number_format($article_actuel->stock_actuel_boraplast,0,',', ' ') }}</th>

                            <?php
                            $nombre_totale_by_article=0;
                            $qt_totale_by_article=0;
                            $nombre_totale_distrubuer_by_article=0;

                            $index_col = 1;
                            ?>
                            @foreach ($date['schedule'] as $date_semaine)
                                <?php
                                if(isset($previsions[ $article_actuel->article_id ][ $date_semaine['semaine'] ])) {
                                    $nb_pallete = $previsions[ $article_actuel->article_id ][ $date_semaine['semaine'] ];
                                } else {
                                    $nb_pallete = 0;
                                }

                                $nb_pallete_total = $nb_pallete_total + $nb_pallete;
                                $quantite = $nb_pallete * $article_actuel->conditionnement;

                                $quantite_total = $quantite_total + $quantite;

                                if( $date_semaine['anne'] == date('Y') &&  intval($date_semaine['semaine']) <= $week_current) {
                                    $activation_update_2 = 0;
                                } else {
                                    $activation_update_2 = 1;
                                }


                                $nombre_totale_by_article+=$nb_pallete;
                                $qt_totale_by_article+=$quantite;

                                ?>
                                <th class="seperator"></th>
                                <th class="quantite" data-toggle="tooltip" title="Qté : {{ $array_col_indicateur[$index_col] }}"><span id="quantite_{{ $article_actuel->article_id }}_{{ $date_semaine['anne'] }}_{{ $date_semaine['semaine'] }}">{{ number_format($quantite,0,',', ' ') }}</span></th>

                                @if($activation_update_2)
                                    <td data-artile="{{ $article_actuel->article_id }}" data-day-year="{{ $date_semaine['anne'] }}_{{ $date_semaine['semaine'] }}" id="nb_palettes_{{ $article_actuel->article_id }}_{{ $date_semaine['anne'] }}_{{ $date_semaine['semaine'] }}" class="quantite-sansborder" data-toggle="tooltip" title="NP : {{ $array_col_indicateur[$index_col] }}">{{ $nb_pallete }}</td>
                                @else
                                    <th data-artile="{{ $article_actuel->article_id }}" data-day-year="{{ $date_semaine['anne'] }}_{{ $date_semaine['semaine'] }}" id="nb_palettes_{{ $article_actuel->article_id }}_{{ $date_semaine['anne'] }}_{{ $date_semaine['semaine'] }}" class="disabled quantite-sansborder" data-toggle="tooltip" title="NP : {{ $array_col_indicateur[$index_col] }}">{{ $nb_pallete }}</th>
                                @endif

                                    <th data-artile="{{ $article_actuel->article_id }}" data-day-year="{{ $date_semaine['anne'] }}_{{ $date_semaine['semaine'] }}" id="nb_palettes_reparti_{{ $article_actuel->article_id }}_{{ $date_semaine['anne'] }}_{{ $date_semaine['semaine'] }}" class="text-center quantite-avecborder" data-toggle="tooltip" title="NPR  : {{ $array_col_indicateur[$index_col] }}"><a href="javascript:void(0)" data-toggle="modal" data-target="#modal_reparti_{{ $article_actuel->article_id }}_{{ $date_semaine['anne'] }}_{{ $date_semaine['semaine'] }}"><span class="np-pcommande-list np-by-semaine-{{ $date_semaine['semaine'] }} badge" data-semaine-reparti="{{ $article_actuel->article_id }}_{{ $date_semaine['anne'] }}_{{ $date_semaine['semaine'] }}">0</span></a></th>

                                    <div id="modal_reparti_{{ $article_actuel->article_id }}_{{ $date_semaine['anne'] }}_{{ $date_semaine['semaine'] }}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog  modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close cancel-repartition" data-dismiss="modal" aria-hidden="true" data-article="{{ $article_actuel->article_id }}" data-anne="{{ $date_semaine['anne'] }}" data-semaine="{{ $date_semaine['semaine'] }}">×</button>
                                                    <h4 class="modal-title" id="myLargeModalLabel">Les défalcations journalières pour l'article <strong>{{ $article_actuel->reference }}</strong></h4>
                                                </div>
                                                <div class="modal-body">
                                                    <h4><strong>Semaine {{ $date_semaine['semaine'] }} : </strong>
                                                            Du {{ $date_semaine['debut_semaine']->format('d/m/Y') }}  Au {{ $date_semaine['fin_semaine']->format('d/m/Y') }}</h4>

                                                    <form id="form-repartition-{{ $article_actuel->article_id }}-{{ $date_semaine['anne'] }}-{{ $date_semaine['semaine'] }}" method="post" action="{{ route('repartition.save') }}" data-article="{{ $article_actuel->article_id }}" data-anne="{{ $date_semaine['anne'] }}" data-semaine="{{ $date_semaine['semaine'] }}">
                                                        {{ csrf_field() }}
                                                        <?php $dat_index = 1; ?>
                                                        <?php $total_prevision_reparti = 0; ?>
                                                        @foreach ($jours_semaine[$date_semaine['semaine']] as $date_jour_semaine)
                                                            <div class="col-xs-12 col-sm-12 col-md-4">
                                                                @if($input_journee_commande_activier[$date_semaine['semaine']][$date_jour_semaine->format('Y-m-d')] && !$data_commande_confirmer[$article_actuel->article_id][$date_jour_semaine->format('Y-m-d')])
                                                                <label for="nb_palettes_{{ $article_actuel->article_id }}_{{ $date_semaine['anne'] }}_{{ $date_semaine['semaine'] }}_{{ $dat_index }}" class="col-12 col-form-label ">{{ strtoupper(substr($date_jour_semaine->format('l'), 0, 3)) }} {{ $date_jour_semaine->format('j') }}/{{ $date_jour_semaine->format('n') }}:</label>
                                                                @else
                                                                    <label for="nb_palettes_{{ $article_actuel->article_id }}_{{ $date_semaine['anne'] }}_{{ $date_semaine['semaine'] }}_{{ $dat_index }}" class="col-12 col-form-label text-desactive">{{ strtoupper(substr($date_jour_semaine->format('l'), 0, 3)) }} {{ $date_jour_semaine->format('j') }}/{{ $date_jour_semaine->format('n') }}:</label>
                                                                @endif

                                                                <div class="col-12">
                                                                    <input type="hidden" name="date_day_{{ $dat_index }}" value="{{ $date_jour_semaine->format('Y-m-d') }}">

                                                                    @if($input_journee_commande_activier[$date_semaine['semaine']][$date_jour_semaine->format('Y-m-d')] && !$data_commande_confirmer[$article_actuel->article_id][$date_jour_semaine->format('Y-m-d')])
                                                                        <input type="number" class="form-control input_commande_repartition" id="nb_palettes_{{ $article_actuel->article_id }}_{{ $date_semaine['anne'] }}_{{ $date_semaine['semaine'] }}_{{ $dat_index }}" value="{{ $data_journee_commande_prevision[$article_actuel->article_id][$date_jour_semaine->format('Y-m-d')] }}" name="nb_palettes_day_{{ $dat_index }}" data-semaine-reparti="{{ $article_actuel->article_id }}_{{ $date_semaine['anne'] }}_{{ $date_semaine['semaine'] }}" data-reset="{{ $data_journee_commande_prevision[$article_actuel->article_id][$date_jour_semaine->format('Y-m-d')] }}">
                                                                    @else
                                                                        <input type="number" class="form-control " disabled="disabled" id="disabled_nb_palettes_{{ $article_actuel->article_id }}_{{ $date_semaine['anne'] }}_{{ $date_semaine['semaine'] }}_{{ $dat_index }}" value="{{ $data_journee_commande_prevision[$article_actuel->article_id][$date_jour_semaine->format('Y-m-d')] }}"  >
                                                                        <input type="hidden" class="input_commande_repartition" id="nb_palettes_{{ $article_actuel->article_id }}_{{ $date_semaine['anne'] }}_{{ $date_semaine['semaine'] }}_{{ $dat_index }}" value="{{ $data_journee_commande_prevision[$article_actuel->article_id][$date_jour_semaine->format('Y-m-d')] }}"  name="nb_palettes_day_{{ $dat_index }}" data-semaine-reparti="{{ $article_actuel->article_id }}_{{ $date_semaine['anne'] }}_{{ $date_semaine['semaine'] }}" data-reset="{{ $data_journee_commande_prevision[$article_actuel->article_id][$date_jour_semaine->format('Y-m-d')] }}">
                                                                    @endif
                                                                </div>

                                                            </div>
                                                            <?php
                                                            $total_prevision_reparti += $data_journee_commande_prevision[$article_actuel->article_id][$date_jour_semaine->format('Y-m-d')];
                                                            $dat_index++;
                                                            ?>
                                                        @endforeach

                                                        <?php
                                                        $nombre_totale_distrubuer_by_article+=$total_prevision_reparti;
                                                        ?>

                                                            <div class="col-xs-12 col-sm-12 col-md-12">
                                                                <hr>
                                                                <h4>Prévisions mensuelle pour semaine {{ $date_month->format('W / Y') }}  = <strong id="text_pm_{{ $article_actuel->article_id }}_{{ $date_semaine['anne'] }}_{{ $date_semaine['semaine'] }}">{{ $nb_pallete }}</strong></h4>
                                                                <h4>Nombre de palettes total selon les défalcations journalières = <strong id="text_pr_{{ $article_actuel->article_id }}_{{ $date_semaine['anne'] }}_{{ $date_semaine['semaine'] }}">{{ $total_prevision_reparti }}</strong></h4>
                                                            </div>

                                                            <input type="hidden" name="id_article" value="{{ $article_actuel->article_id }}">
                                                            <input type="hidden" id="np_prevision_m_{{ $article_actuel->article_id }}_{{ $date_semaine['anne'] }}_{{ $date_semaine['semaine'] }}" value="{{ $nb_pallete }}">

                                                    </form>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" id="close_modal_{{ $article_actuel->article_id }}_{{ $date_semaine['anne'] }}_{{ $date_semaine['semaine'] }}" class="btn btn-default waves-effect cancel-repartition" data-dismiss="modal" data-article="{{ $article_actuel->article_id }}" data-anne="{{ $date_semaine['anne'] }}" data-semaine="{{ $date_semaine['semaine'] }}">Annuler</button>
                                                    <button id="send_repartition_{{ $article_actuel->article_id }}_{{ $date_semaine['anne'] }}_{{ $date_semaine['semaine'] }}" type="button" class="btn btn-danger waves-effect waves-light send-repartition" data-ajax="form-repartition-{{ $article_actuel->article_id }}-{{ $date_semaine['anne'] }}-{{ $date_semaine['semaine'] }}">Confirmer</button>
                                                </div>
                                            </div>
                                            <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>

                                    <?php
                                    $index_col++;
                                    ?>

                            @endforeach

                            <th class="seperator"></th>
                            <th class="text-center" data-toggle="tooltip" title="Total Qté">{{ number_format($qt_totale_by_article, 0, ',', ' ') }}</th>
                            <th class="text-center" data-toggle="tooltip" title="Total NP">{{ $nombre_totale_by_article }}</th>
                            <th class="text-center" data-toggle="tooltip" title="Total NPR">{{ $nombre_totale_distrubuer_by_article }}</th>
                            <?php
                            $nombre_totale_by_all_article+=$nombre_totale_by_article;
                            $qt_totale_by_all_article+=$qt_totale_by_article;
                            $nombre_totale_distrubuer_by_all_article+=$nombre_totale_distrubuer_by_article;
                            ?>

                        </tr>

                    @endforeach



                    </tbody>
                    <tfoot>
                    <tr>
                        <th class="active">Total</th>
                        <th class="active"></th>
                        <th class="active"></th>
                        <?php
                        $index_col = 1;
                        ?>
                        @foreach ($date['schedule'] as $date_semaine)

                            <?php
                            if(isset($data_previsions_total[ $date_semaine['semaine'] ]['quantittes'])) {
                                $total_quantity =  $data_previsions_total[ $date_semaine['semaine'] ]['quantittes'];
                            } else {
                                $total_quantity =  0;
                            }

                            if(isset($data_previsions_total[ $date_semaine['semaine'] ]['nombre_palettes'])) {
                                $total_nombre_palettes = $data_previsions_total[ $date_semaine['semaine'] ]['nombre_palettes'];
                            } else {
                                $total_nombre_palettes =  0;
                            }
                            ?>
                            <th class="seperator"></th>
                            <th data-toggle="tooltip" title="Total Qté : {{ $array_col_indicateur[$index_col] }}" class="quantite" id="total_quantitees_{{ $date_semaine['anne'] }}_{{ $date_semaine['semaine'] }}">{{ number_format($total_quantity,0,',', ' ') }}</th>
                            <th data-toggle="tooltip" title="Total NP : {{ $array_col_indicateur[$index_col] }}" class="active quantite-sansborder" id="total_nb_palettes_{{ $date_semaine['anne'] }}_{{ $date_semaine['semaine'] }}">{{ $total_nombre_palettes }}</th>
                            <th data-toggle="tooltip" title="Total NPR : {{ $array_col_indicateur[$index_col] }}" class="active quantite-avecborder" id="total_nb_palettes_reparti_{{ $date_semaine['anne'] }}_{{ $date_semaine['semaine'] }}">{{ $total_nombre_palettes_by_semaine[$date_semaine['semaine']] }}</th>
                            <?php
                            $index_col++;
                            ?>
                        @endforeach
                        <th class="seperator"></th>
                        <th>{{ number_format($qt_totale_by_all_article,0,',', ' ') }}</th>
                        <th>{{ $nombre_totale_by_all_article }}</th>
                        <th>{{ $nombre_totale_distrubuer_by_all_article }}</th>

                    </tr>
                    <tr>
                        <th colspan="3"></th>
                        @foreach ($date['schedule'] as $date_semaine)

                            <?php
                            if( $date_semaine['anne'] == date('Y') && intval($date_semaine['semaine']) <= $week_current) {
                                $activation_update_2 = 0;
                            } else {
                                $activation_update_2 = 1;
                            }
                            ?>
                                <th class="seperator"></th>
                        <th colspan="3" class="active buttons">
                            @if($activation_update_2)
                                <form id="form-commande-{{ $date_semaine['anne'] }}_{{ $date_semaine['semaine'] }}" method="post" action="{{ route('previsions.create2') }}">
                                    {{ csrf_field() }}

                                    <input type="hidden" name="nombre_semaine" value="{{ $date_semaine['semaine'] }}">
                                    <input type="hidden" name="nombre_anne" value="{{ $date_semaine['anne'] }}">

                                    @foreach ($articles_actuels as $article_actuel)
                                        <?php
                                        if(isset($previsions[ $article_actuel->article_id ][ $date_semaine['semaine'] ])) {
                                            $nb_pallete = $previsions[ $article_actuel->article_id ][ $date_semaine['semaine'] ];
                                        } else {
                                            $nb_pallete = 0;
                                        }
                                        ?>

                                        <input type="hidden" id="{{ $article_actuel->article_id }}_{{ $date_semaine['anne'] }}_{{ $date_semaine['semaine'] }}" name="quantitee_article_{{ $article_actuel->article_id }}_{{ $date_semaine['anne'] }}_{{ $date_semaine['semaine'] }}" value="{{ $nb_pallete }}">

                                    @endforeach

                                </form>
                                <button id="send-form-{{ $date_semaine['anne'] }}_{{ $date_semaine['semaine'] }}" class="btn btn-info btn-md btn-block send-commande" data-ajax="form-commande-{{ $date_semaine['anne'] }}_{{ $date_semaine['semaine'] }}" data-day="{{ $date_semaine['anne'] }}_{{ $date_semaine['semaine'] }}" data-url-order="{{ route('commandes', $date_semaine['semaine']) }}">Confirm Prévision</button>

                            @endif

                        </th>
                        @endforeach

                        <th class="seperator"></th>
                    </tr>
                    <tr>
                        <th colspan="3"></th>
                        @foreach ($date['schedule'] as $date_semaine)

                            <?php
                            if( intval($date_semaine['semaine']) <= $week_current) {
                                $activation_update_2 = 0;
                            } else {
                                $activation_update_2 = 1;
                            }
                            ?>
                                <th class="seperator"></th>
                            <th colspan="3" class="active buttons" id="containe-cell-{{ $date_semaine['anne'] }}_{{ $date_semaine['semaine'] }}">
                                @if($data_confirmation[$date_semaine['semaine']])
                                    <a href="{{ route('commandes', ['nombre_semaine' => $date_semaine['semaine'], 'nombre_anne' => $date_semaine['anne']]) }}" class="btn btn-info btn-md btn-block">Commander</a>

                                @endif
                            </th>

                        @endforeach

                        <th class="seperator"></th>
                    </tr>
                    </tfoot>
                </table>
                </div>

            </div>
        </div>

    </div>
    <!-- /.row -->



@endsection

@section('scripts')

    <!-- Sweet-Alert  -->
    <script src="{{ asset('backend/plugins/bower_components/sweetalert/sweetalert.min.js') }}"></script>
    <script src="{{ asset('backend/plugins/bower_components/sweetalert/jquery.sweet-alert.custom.js') }}"></script>

    <script src="{{ asset('backend/eliteadmin/js/custom.min.js') }}"></script>

    <!-- Editable -->
    <script src="{{ asset('backend/plugins/bower_components/jquery-datatables-editable/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('backend/plugins/bower_components/datatables/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('backend/plugins/bower_components/tiny-editable/mindmup-editabletable.js') }}"></script>
    <script src="{{ asset('backend/plugins/bower_components/tiny-editable/numeric-input-example.js') }}"></script>
    <script>
        $('#editable-datatable').editableTableWidget({ headerCols: true }).numericInputExample().find('td:first').focus();
        $(document).ready(function() {

            $('td[readonly]').bind('click dblclick',function(e) {
                e.preventDefault();
                e.stopPropagation();
            });

            $('#editable-datatable td').on('change', function(evt, newValue) {
                // do something with the new cell value

                if(newValue<0) {
                    evt.stopPropagation();
                    evt.preventDefault();
                    return false;
                }

                var np = $(this).text();
                var _idArtile = $(this).attr("data-artile");
                var _dayYear = $(this).attr("data-day-year");

                var _conditionnement = $('#conditionnement_'+_idArtile).val();

                var _quantite = parseInt(_conditionnement) * parseInt(np);


                $("#quantite_"+_idArtile+"_"+_dayYear).text(number_format(_quantite, 0, ',' , ' '));

                $("#"+_idArtile+"_"+_dayYear).val(np);

                setTotalQuantitee(_dayYear);

                changeEtat(_idArtile, _dayYear);


            });

            function setTotalQuantitee(_dayYear) {

                var _total_quantitee = 0;

                $('#editable-datatable > tbody  > tr').each( function() {
                        var _idArtile = $(this).attr("id");

                    _total_quantitee += parseInt($("#quantite_"+_idArtile+"_"+_dayYear).text().replace(' ',''));

                });

                $("#total_quantitees_"+_dayYear).text(number_format(_total_quantitee, 0, ',' , ' '));
            }

            /**********************************
             *
             * Chnagement les input de repartition**
             *
             * text_pr_3_2017_31
             *
             */
            $(".input_commande_repartition").bind("change paste keyup", function() {
                var _inputUpdate = $(this);
                var _valeurUpdate = _inputUpdate.val();


                if(parseInt(_valeurUpdate) < 0) {
                    _inputUpdate.val('0');
                }

                var _dataSemaineReparti = $(this).attr('data-semaine-reparti');

                cotroleSomme(_dataSemaineReparti);
            });

            $('.send-repartition').click(function() {

                var varForm = $(this).attr('data-ajax');
                var form = $('#' + varForm);

                var _anne = form.attr('data-anne');
                var _semaine = form.attr('data-semaine');
                var _article = form.attr('data-article');

                var day_number = $(this).attr('data-day');

                swal({
                    title: "Etes vous sûre de vouloir valider les défalcations journalières ?",
                    text: "NB: Vous pouvez modifier votre répartitions à tout moment.",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes",
                    closeOnConfirm: false,
                    showLoaderOnConfirm: true

                }, function () {
                    $.ajax({
                        type: 'post',
                        url: form.attr('action'),
                        data: form.serialize(),
                        dataType: 'json',
                        success: function (data) {
                            // success logic
                            //console.log(data);

                            if (data.status == 'success') {
                                swal("Enregistré!", "Votre répartitions a été enregistré avec succès.", "success");

                                // update valeur NP
                                //
                                //update couleur selon le %
                                var _sum = $('#text_pr_' +_article+'_'+_anne+'_'+_semaine ).text();
                                $('#nb_palettes_reparti_'+_article+'_'+_anne+'_'+_semaine).find('.np-pcommande-list').text(_sum);

                                // update data-reset for input np detaill
                                var _dataSemaineReparti = _article + '_' + _anne + '_' + _semaine;
                                var arr = [ 1, 2, 3, 4, 5, 6, 7];
                                $.each( arr, function( index, value ){
                                    var _newValeurInput = $('#nb_palettes_' + _dataSemaineReparti + '_' + value).val();
                                    $('#nb_palettes_' + _dataSemaineReparti + '_' + value).attr('data-reset',_newValeurInput);

                                });

                                cotroleSommeSemaine(_anne, _semaine);

                                // close modal
                                $("#modal_reparti_"+_article+'_'+_anne+'_'+_semaine).modal('toggle');

                            } else {
                                console.log(data);
                                swal("Erreur!", "PB DATABASE.", "error");
                            }
                        },
                        error: function (data) {
                            console.log(data);
                            swal("Erreur!", "PB REQUEST.", "error");
                        }
                    });
                });
            });

            $('.cancel-repartition').click(function(){
                var _anne = $(this).attr('data-anne');
                var _semaine = $(this).attr('data-semaine');
                var _article = $(this).attr('data-article');

                var form = $('#form-repartition-' + _article + '-' + _anne + '-' +_semaine);
                //reset les inputs
                resetForm(_article, _anne, _semaine);

                var _dataSemaineReparti = _article + '_' + _anne + '_' + _semaine;

                cotroleSomme(_dataSemaineReparti);

            });

            function resetForm(_article, _anne, _semaine) {
                var _dataSemaineReparti = _article + '_' + _anne + '_' + _semaine;
                var arr = [ 1, 2, 3, 4, 5, 6, 7];

                $.each( arr, function( index, value ){
                    var _oldValeurInput = $('#nb_palettes_' + _dataSemaineReparti + '_' + value).attr('data-reset');

                    $('#nb_palettes_' + _dataSemaineReparti + '_' + value).val(_oldValeurInput)

                });
            }

            function cotroleSomme(_dataSemaineReparti) {
                var arr = [ 1, 2, 3, 4, 5, 6, 7];

                var _np_prevesion_mensuel = $('#text_pm_' + _dataSemaineReparti).text();
                _np_prevesion_mensuel = parseInt(_np_prevesion_mensuel);
                var sum = 0;

                $.each( arr, function( index, value ){
                    var valeur_input = $('#nb_palettes_' + _dataSemaineReparti + '_' + value).val();

                    valeur_input = parseInt(valeur_input);

                    if(valeur_input>0) {
                        sum += valeur_input;
                    }

                });

                if(sum == _np_prevesion_mensuel) {
                    // display boutton confirmation
                    // vers;

                    $('#nb_palettes_reparti_'+_dataSemaineReparti).find('.badge').removeClass('badge-danger').addClass('badge-success');
                } else {
                    // cacher boutton confirmation
                    $('#nb_palettes_reparti_'+_dataSemaineReparti).find('.badge').removeClass('badge-success').addClass('badge-danger');
                }

                $('#text_pr_' + _dataSemaineReparti ).text(sum);
            }

            function changeEtat(_id_article, _dataYear) {
                var nb_palettes_en_prevision  = parseInt($('#nb_palettes_'+_id_article+'_'+_dataYear).text());
                var nb_palettes_en_repartition  = parseInt($('#nb_palettes_reparti_'+_id_article+'_'+_dataYear).text());

                if(nb_palettes_en_prevision == nb_palettes_en_repartition) {
                    $('#nb_palettes_reparti_'+_id_article+'_'+_dataYear).find('.badge').removeClass('badge-danger').addClass('badge-success');
                } else {
                    $('#nb_palettes_reparti_'+_id_article+'_'+_dataYear).find('.badge').removeClass('badge-success').addClass('badge-danger');
                }

            }

            function cotroleSommeSemaine(_anne, _semaine) {

                var np_semaine_total = 0;
                $('.np-by-semaine-' + _semaine).each( function() {
                    var np_article_semaine = $(this).text();
                    console.log('fffff = ' + np_article_semaine);

                    np_semaine_total += parseInt(np_article_semaine);
                });

                $('#total_nb_palettes_reparti_'+_anne+'_'+_semaine).text(np_semaine_total)
            }


            // pour recuperer les sommes de nb pallete et mettre sur le tableau
            $('.np-pcommande-list').each(function(i, obj) {
                var idInfoReparti = $(this).attr('data-semaine-reparti');

                var np = $('#text_pr_'+idInfoReparti).text();
                np = parseInt(np);

                var np_en_prevision = $('#nb_palettes_'+idInfoReparti).text();
                np_en_prevision = parseInt(np_en_prevision);
                if(np_en_prevision == np) {
                    $('#nb_palettes_reparti_'+idInfoReparti).find('.badge').addClass('badge-success');
                } else {
                    $('#nb_palettes_reparti_'+idInfoReparti).find('.badge').addClass('badge-danger');
                }

                $(this).text(np);
            });



            //Warning Message
            $('.send-commande').click(function(){

                var varBtn = $(this);
                var varForm = $(this).attr('data-ajax');
                var varUrlOrder = $(this).attr('data-url-order');
                var form = $('#'+varForm);

                var day_number = $(this).attr('data-day');

                swal({
                    title: "Etes vous sûre de vouloir valider la prévision ?",
                    text: "Vous pouvez modifier votre prévision à tout moment.",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes",
                    closeOnConfirm: false,
                    showLoaderOnConfirm: true

                }, function(){
                    $.ajax({
                        type: 'post',
                        url: form.attr( 'action' ),
                        data: form.serialize(),
                        dataType: 'json',
                        success: function(data) {
                            // success logic
                            console.log(data);

                            if(data.status == 'success') {
                                swal("Enregistré!", "Vos quantités prévisionnelles ont bien été enregistrer. Vous recevrez bientôt un mail de suivi.", "success");
                                //viderCol(day_number);
                                //varBtn.remove();

                                //var url_order = '<a href="'+varUrlOrder+'" class="btn btn-info btn-md btn-block">Détails</a>';
                                //$('#containe-cell-' + day_number).html(url_order);

                            } else {
                                console.log(data);
                                swal("Erreur!", "PB DATABASE.", "error");
                            }
                        },
                        error: function(data){
                            console.log(data);
                            swal("Erreur!", "PB REQUEST.", "error");
                        }
                    });
                });
            });


            function number_format (number, decimals, dec_point, thousands_sep) {
                // Strip all characters but numerical ones.
                number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
                var n = !isFinite(+number) ? 0 : +number,
                    prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
                    sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
                    dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
                    s = '',
                    toFixedFix = function (n, prec) {
                        var k = Math.pow(10, prec);
                        return '' + Math.round(n * k) / k;
                    };
                // Fix for IE parseFloat(0.55).toFixed(0) = 0;
                s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
                if (s[0].length > 3) {
                    s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
                }
                if ((s[1] || '').length < prec) {
                    s[1] = s[1] || '';
                    s[1] += new Array(prec - s[1].length + 1).join('0');
                }
                return s.join(dec);
            }


        });
    </script>

@endsection


