{{-- \resources\views\users\edit.blade.php --}}

@extends('layouts.app')

@section('title', '| Edit User')

@section('content')

    <div class="row">
        <div class="col-lg-6">
            <div class="white-box">
                <h3 class="box-title m-b-0"><i class='fa fa-user-plus'></i> Edit User {{$user->name}}</h3>
                {{ Form::model($user, array('route' => array('users.update', $user->id), 'method' => 'PUT', 'class' => 'form-material form-horizontal m-t-30')) }}{{-- Form model binding to automatically populate our fields with user data --}}

                <div class="form-group">
                    {{ Form::label('name', 'Name') }}
                    {{ Form::text('name', null, array('class' => 'form-control')) }}
                </div>

                <div class="form-group">
                    {{ Form::label('email', 'Email') }}
                    {{ Form::email('email', null, array('class' => 'form-control')) }}
                </div>

                <div class="form-group">
                    {{ Form::label('code_client', 'Code Client') }}
                    {{ Form::text('code_client', null, array('class' => 'form-control')) }}
                </div>

                <div class="form-group">
                    {{ Form::label('nom_societe', 'Nom de la société') }}
                    {{ Form::text('nom_societe', null, array('class' => 'form-control')) }}
                </div>

                <div class="form-group">
                    {{ Form::label('', 'Jour de commande') }}

                    <h5>Selon les jours spécifique</h5>

                    @foreach ($days as $day)

                        {{ Form::checkbox('days_commande[]', $day, null, array('id'=>'day_'.$day)) }}
                        {{ Form::label('day_'.$day, ucfirst($day)) }}<br>


                    @endforeach

                    <br>

                    <h5>{{ Form::label('jour_commande', 'Selon les jours plus N') }}</h5>

                    {{ Form::select('jour_commande',[
                                '1' => 'Jour + 1',
                                '2' => 'Jour + 2',
                                '3' => 'Jour + 3',
                                '4' => 'Jour + 4',
                                '5' => 'Jour + 5',
                                '6' => 'Jour + 6',
                                ], null, array('class' => 'form-control'))
                            }}


                    <div class="col-xs-12">

                        <div class="col-xs-12 col-sm-5">

                        </div>

                        <div class="col-xs-12 col-sm-5">

                        </div>
                    </div>



                </div>



                <h5><b>Assigner un role</b></h5>

                <div class='form-group'>
                    @foreach ($roles as $role)
                        {{ Form::checkbox('roles[]',  $role->id, $user->roles ) }}
                        {{ Form::label($role->name, ucfirst($role->name)) }}<br>

                    @endforeach
                </div>

                {{ Form::submit('Changer', array('class' => 'btn btn-primary')) }}

                {{ Form::close() }}
            </div>

        </div>

        <div class="col-lg-6">
            <div class="white-box">
                <h3 class="box-title m-b-0"><i class='fa fa-user-plus'></i> Changer Mot de pass -  {{$user->name}}</h3>
                {{ Form::model($user, array('route' => array('users.updatePass', $user->id), 'method' => 'PUT', 'class' => 'form-material form-horizontal m-t-30')) }}{{-- Form model binding to automatically populate our fields with user data --}}

                <div class="form-group">
                    {{ Form::label('password', 'Password') }}<br>
                    {{ Form::password('password', array('class' => 'form-control')) }}

                </div>

                <div class="form-group">
                    {{ Form::label('password', 'Confirm Password') }}<br>
                    {{ Form::password('password_confirmation', array('class' => 'form-control')) }}

                </div>

                {{ Form::submit('Changer', array('class' => 'btn btn-primary')) }}

                {{ Form::close() }}
            </div>



            <div class="white-box gris-box">
                <h3 class="box-title m-b-0"><i class='fa fa-user-plus'></i> Logo de la société </h3>
                {{ Form::model($user,
                    array(
                    'route' => array('users.updateLogo', $user->id),
                    'method' => 'PUT',
                    'class' => 'form-material form-horizontal m-t-30',
                    'files' => true)) }}

                <div class="form-group">
                    {{ Form::file('logo_societe', null, array('class' => 'form-control')) }}

                </div>

                @if($user->logo_societe)
                <div class="form-group">
                    <img src="{{ asset($user->logo_societe) }}" class="img-responsive">
                </div>
                @endif

                {{ Form::submit('Changer', array('class' => 'btn btn-primary')) }}

                {{ Form::close() }}
            </div>

        </div>
    </div>

@endsection

@section('scripts')
    <script src="{{ asset('backend/eliteadmin/js/custom.min.js') }}"></script>

    <script>
        $(document).ready(function() {


        });
    </script>

@endsection