{{-- \resources\views\users\create.blade.php --}}
@extends('layouts.app')

@section('title', 'Ajouter un utilisateur')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="white-box">
                <h3 class="box-title"><i class='fa fa-user-plus'></i> Nouveau utilisateur</h3>
                {{ Form::open(array('url' => 'users', 'files' => true, 'class' => 'form-material form-horizontal m-t-30')) }}

                <div class="form-group">
                    {{ Form::label('name', 'Nom') }}
                    {{ Form::text('name', '', array('class' => 'form-control')) }}
                </div>

                <div class="form-group">
                    {{ Form::label('email', 'Email') }}
                    {{ Form::email('email', '', array('class' => 'form-control')) }}
                </div>


                <div class="form-group">
                    {{ Form::label('code_client', 'Code Client') }}
                    {{ Form::text('code_client', '', array('class' => 'form-control')) }}
                </div>

                <div class="form-group">
                    {{ Form::label('nom_societe', 'Nom de la société') }}
                    {{ Form::text('nom_societe', '', array('class' => 'form-control')) }}
                </div>

                <div class="form-group">
                    {{ Form::label('', 'Jour de commande') }}

                    <h5>Selon les jours spécifique</h5>

                    @foreach ($days as $day)

                        {{ Form::checkbox('days_commande[]', $day, null, array('id'=>'day_'.$day)) }}
                        {{ Form::label('day_'.$day, ucfirst($day)) }}<br>


                    @endforeach

                    <br>

                    <h5>{{ Form::label('jour_commande', 'Selon les jours plus N') }}</h5>

                    {{ Form::select('jour_commande',[
                                '1' => 'Jour + 1',
                                '2' => 'Jour + 2',
                                '3' => 'Jour + 3',
                                '4' => 'Jour + 4',
                                '5' => 'Jour + 5',
                                '6' => 'Jour + 6',
                                ], null, array('class' => 'form-control'))
                            }}


                    <div class="col-xs-12">

                        <div class="col-xs-12 col-sm-5">

                        </div>

                        <div class="col-xs-12 col-sm-5">

                        </div>
                    </div>



                </div>

                <h5><b>Assigner un role</b></h5>

                <div class='form-group'>
                    @foreach ($roles as $role)
                        {{ Form::checkbox('roles[]',  $role->id, null, array('id'=>'role_'.$role->id) ) }}
                        {{ Form::label('role_'.$role->id, ucfirst($role->name)) }}<br>

                    @endforeach
                </div>

                <div class="form-group">
                    {{ Form::label('password', 'Password') }}<br>
                    {{ Form::password('password', array('class' => 'form-control')) }}

                </div>

                <div class="form-group">
                    {{ Form::label('password', 'Confirm Password') }}<br>
                    {{ Form::password('password_confirmation', array('class' => 'form-control')) }}

                </div>

                {{ Form::submit('Add', array('class' => 'btn btn-primary')) }}

                {{ Form::close() }}
            </div>
        </div>
    </div>

@endsection


@section('scripts')
    <script src="{{ asset('backend/eliteadmin/js/custom.min.js') }}"></script>

    <script>
        $(document).ready(function() {


        });
    </script>

@endsection