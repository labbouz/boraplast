<li> <a href="#" class="waves-effect active_prevesion_mensuel"><i data-icon="f" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu"> Répartition <strong>30 J</strong> <span class="fa arrow"></span></span></a>

    <ul class="nav nav-second-level">
        <?php
        $monthOfYear = $date_current->format('n');

        $date_current->add('1 month');
        ?>
        <li> <a href="{{ route('previsions', ['anne' => $date_current->format('Y'), 'nombre_mois' => $date_current->format('n')] ) }}"><span class="first_upper">Mois {{ $date_current->format('F - Y') }} </span> (suivant)</a> </li>
        @for ($m = $monthOfYear; $m > 0; $m--)
            <?php $date_current->sub('1 month') ?>
            <li> <a href="{{ route('previsions', ['anne' => $date_current->format('Y'), 'nombre_mois' => $date_current->format('n')] ) }}">Mois <span class="first_upper">{{ $date_current->format('F - Y') }}</span>
                    @if($monthOfYear == $m)
                        (curent)
                    @endif
                </a> </li>
        @endfor
    </ul>
</li>

@if(! auth()->user()->hasAnyRole(['Admin', 'Client']))

<li> <a href="#" class="waves-effect active_prevesion_hebdomadair"><i data-icon="h" class="linea-icon linea-elaborate fa-fw"></i> <span class="hide-menu"> Répartition  <strong>07 J</strong>   <span class="fa arrow"></span></span></a>

    <ul class="nav nav-second-level">
        <?php
        $monthOfYear = $date_current_hebdo->format('n');



        $date_current_hebdo->add('1 month');
        ?>
            <li> <a href="{{ route('previsions_hebdo', ['anne' => $date_current_hebdo->format('Y'), 'nombre_mois' => $date_current_hebdo->format('n')] ) }}"><span class="first_upper">Mois {{ $date_current_hebdo->format('F - Y') }} </span> (suivant)</a> </li>
        @for ($m = $monthOfYear; $m > 0; $m--)
                <?php $date_current_hebdo->sub('1 month') ?>
            <li> <a href="{{ route('previsions_hebdo', ['anne' => $date_current_hebdo->format('Y'), 'nombre_mois' => $date_current_hebdo->format('n')] ) }}">Mois <span class="first_upper">{{ $date_current_hebdo->format('F - Y') }}</span>
                    @if($monthOfYear == $m)
                        (curent)
                    @endif
                   </a> </li>
        @endfor
    </ul>
</li>
@endif



