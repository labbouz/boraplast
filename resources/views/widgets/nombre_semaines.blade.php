@if(auth()->user()->hasAnyRole(['Admin', 'Client']))
<li> <a href="#" class="waves-effect active_commande"><i data-icon="&#xe01a;" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu"> Commande <strong>CDE</strong> <span class="fa arrow"></span></span></a>

    <ul class="nav nav-second-level">
        <li><a href="{{ route('commandes', ['nombre_semaine' => $weekOfYear+1, 'nombre_anne' => $Year]) }}">Semaine {{ $weekOfYear+1 }} (suivante)</a> </li>
        <li><a href="{{ route('commandes', ['nombre_semaine' => $weekOfYear+1, 'nombre_anne' => $Year] ) }}">Semaine {{ $weekOfYear }} (courante)</a> </li>
    </ul>
</li>
<li> <a href="#" class="waves-effect active_commande"><i data-icon="R" class="linea-icon linea-ecommerce fa-fw"></i> <span class="hide-menu"> Commande <strong>historique</strong> <span class="fa arrow"></span></span></a>
    <ul class="nav nav-second-level">
        <li><a href="{{ route('historique.commandes', $weekOfYear+1) }}">Semaine {{ $weekOfYear+1 }}</a> </li>
        <li><a href="{{ route('historique.commandes', $weekOfYear) }}">Semaine {{ $weekOfYear }}</a> </li>
        @for ($s = $weekOfYear-1; $s > 0; $s--)
            <li> <a href="{{ route('historique.commandes', $s) }}">Semaine {{ $s }}</a> </li>
        @endfor
    </ul>
</li>
    @else
    <li> <a href="#" class="waves-effect active_commande"><i data-icon="&#xe01a;" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu"> Commande <strong>CDE</strong> <span class="fa arrow"></span></span></a>

        <ul class="nav nav-second-level">
            <li><a href="{{ route('commandes', ['nombre_semaine' => $weekOfYear+1, 'nombre_anne' => $Year] ) }}">Semaine {{ $weekOfYear+1 }} (suivante)</a> </li>
            <li><a href="{{ route('commandes', ['nombre_semaine' => $weekOfYear, 'nombre_anne' => $Year]) }}">Semaine {{ $weekOfYear }} (courante)</a> </li>
            @for ($s = $weekOfYear-1; $s > 0; $s--)
                <li> <a href="{{ route('commandes', ['nombre_semaine' => $s, 'nombre_anne' => $Year]) }}">Semaine {{ $s }}</a> </li>
            @endfor
        </ul>
    </li>

@endif

<li> <a href="{{ route('file.log') }}" class="waves-effect"><i data-icon="S" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu"> Historique </span></a>
</li>