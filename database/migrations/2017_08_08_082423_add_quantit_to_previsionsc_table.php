<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddQuantitToPrevisionscTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('previsionsc', function (Blueprint $table) {
            $table->integer('conditionnement')->default(0);
            $table->integer('stock_actuel_boraplast')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('previsionsc', function (Blueprint $table) {
            //
        });
    }
}
