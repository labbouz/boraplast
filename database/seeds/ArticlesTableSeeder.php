<?php

use Illuminate\Database\Seeder;

class ArticlesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //disable foreign key check for this connection before running seeders
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        //delete Delegations table records
        DB::table('articles_actuels')->truncate();
        DB::table('commandes')->truncate();
        DB::table('commandes')->truncate();
        DB::table('articles')->truncate();

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
